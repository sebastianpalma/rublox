### Example

```bash
$ ruby lib/lox.rb file.lox
```

See spec/integration/lox_spec.rb for examples.
