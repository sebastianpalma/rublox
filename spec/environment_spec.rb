# frozen_string_literal: true

require_relative './spec_helper'

require_relative './../lib/environment'
require_relative './../lib/errors/runtime_error'
require_relative './../lib/token'
require_relative './../lib/token_types'

RSpec.describe Environment do
  describe '#new' do
    subject { described_class.new }

    context '#enclosing' do
      context 'initialized with no enclosing' do
        it 'has a nil enclosing' do
          expect(subject.enclosing).to eq(nil)
        end
      end

      context 'initialized with enclosing' do
        subject { described_class.new(new_enclosing) }
        
        let(:new_enclosing) { Environment.new }

        it 'has a nil enclosing' do
          expect(subject.enclosing).to eq(new_enclosing)
        end
      end
    end

    it 'has {} values' do
      expect(described_class.values).to eq({})
    end
  end

  describe '#get' do
    subject { env.get(name) }

    let(:env) { described_class.new }
    let(:name) { Token.new('foo', 1, 'nil', TokenTypes::IDENTIFIER) }

    context 'previously defined variable' do
      before { env.values = { 'foo' => 1 } }

      it { expect(subject).to eq(1) }
    end

    context 'when @enclosing.nil?' do
      it { expect { subject }.to raise_error(RuntimeError, "Undefined variable 'foo'.") }
    end

    context 'when !@enclosing.nil?' do
      context 'variable defined previously in @enclosing' do
        before do
          env.instance_variable_set(
            :@enclosing,
            Environment.new.tap do |env|
              env.values = { 'foo' => 'click-clack' }
            end
          )
        end

        it 'extracts it from @enclosing using #get' do
          expect(subject).to eq('click-clack')
        end
      end

      context 'variable not defined previously in @enclosing' do
        before do
          env.instance_variable_set(:@enclosing, Environment.new)
        end

        it { expect { subject }.to raise_error(RuntimeError, "Undefined variable 'foo'.") }
      end
    end

    context 'undefined variable' do
      it { expect { subject }.to raise_error(RuntimeError, "Undefined variable 'foo'.") }
    end
  end

  describe '#assign' do
    subject { env.assign(name, value) }

    let(:env) { described_class.new }
    let(:name) { Token.new('foo', 1, 'nil', TokenTypes::IDENTIFIER) }
    let(:value) { 7 }

    context 'when +@@values+ contain the +name.lexeme+' do
      before do
        env.values = { 'foo' => -1 }
      end

      it 'updates the value of +name.lexeme+ to the given value' do
        subject
        expect(described_class.values).to eq('foo' => value)
      end

      it 'returns nil' do
        expect(subject).to eq(nil)
      end
    end

    context 'when +@@values+ does not contain the +name.lexeme+' do
      context 'undefined variable' do
        it { expect { subject }.to raise_error(RuntimeError, "Undefined variable 'foo'.") }
      end
    end
  end

  describe '#define' do
    subject { env.define('foo', 3) }

    let(:env) { described_class.new }

    it 'assigns the given +name+ and +values into @@values' do
      subject
      expect(env.values).to eq('foo' => 3)
    end

    it 'returns the given value' do
      expect(subject).to eq(3)
    end
  end
end
