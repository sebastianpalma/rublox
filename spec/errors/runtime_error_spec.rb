# frozen_string_literal: true

require_relative './../spec_helper'

require_relative './../../lib/errors/runtime_error'

RSpec.describe RuntimeError do
  subject { described_class.new(message, op) }

  let(:message) { 'hey' }
  let(:op) { :+ }

  it { expect { raise subject }.to raise_error(RuntimeError, 'hey') }
end
