# frozen_string_literal: true

require_relative './../spec_helper'

require_relative './../../lib/errors/parser_error'

RSpec.describe ParserError do
  subject { described_class.new }

  it { expect { raise subject }.to raise_error(ParserError) }
end
