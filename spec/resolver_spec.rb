# frozen_string_literal: true

require_relative './spec_helper'

require_relative './../lib/lox'
require_relative './../lib/resolver'
require_relative './../lib/interpreter'
require_relative './../lib/token'
require_relative './../lib/token_types'

RSpec.describe Resolver do
  # after { described_class.class_variable_set(:@@scopes, []) }

  describe '#begin_scope' do
    subject { described_class.new(Interpreter.new).begin_scope }

    it 'adds an empty hash to +@@scopes+' do
      expect { subject }
        .to change { described_class.class_variable_get(:@@scopes) }
        .from([]).to([{}])
    end
  end

  describe '#end_scope' do
    subject { described_class.new(Interpreter.new).end_scope }

    before do
      described_class.class_variable_set(:@@scopes, [{}])
    end
    
    it 'removes the last element from +@@scopes+ and return it' do
      expect { subject }
        .to change { described_class.class_variable_get(:@@scopes) }
        .from([{}]).to([])

      expect(subject).to eq({})
    end
  end

  describe '#declare' do
    context 'when +@@scopes+ is empty' do
      subject do
        described_class
          .new(Interpreter.new)
          .declare(Token.new(';', 1, 'nil', TokenTypes::SEMICOLON))
      end

      it 'returns nil' do
        expect(subject).to eq(nil)
      end
    end

    context 'when +@@scopes+ is not empty' do
      subject { resolver.declare(name) }

      let(:resolver) { described_class.new(Interpreter.new) }

      context 'when the given +name.lexeme+ is already in +@@scopes+' do
        let(:name) { Token.new('myVar', 1, 'nil', TokenTypes::IDENTIFIER) }

        before do
          described_class.class_variable_set(
            :@@scopes,
            [{'myVar' => false}]
          )
        end

        it 'raises an error' do
          expect { subject }
            .to output(<<~TEXT)
              [line 1] Error at 'myVar': Already a variable with this name in this scope.
            TEXT
            .to_stdout

          expect(subject).to eq(nil)
        end
      end

      context 'when the given +name.lexeme+ is not in +@@scopes+' do
        let(:name) { Token.new('otherVar', 1, 'nil', TokenTypes::IDENTIFIER) }

        before { resolver.begin_scope }

        it 'sets the given +name.lexeme+ within +@@scopes+' do
          expect { subject }
            .to change { described_class.class_variable_get(:@@scopes) }
            .from([{}])
            .to([{ 'otherVar' => false }])

          expect(subject).to eq(nil)
        end
      end
    end
  end

  describe '#define' do
    context 'when +@@scopes+ is empty' do
      subject do
        described_class
          .new(Interpreter.new)
          .define(Token.new(';', 1, 'nil', TokenTypes::SEMICOLON))
      end

      it 'returns nil' do
        expect(subject).to eq(nil)
      end
    end

    context 'when +@@scopes+ is not empty' do
      subject do
        resolver.define(Token.new(';', 1, 'nil', TokenTypes::SEMICOLON))
      end

      let(:resolver) { described_class.new(Interpreter.new) }

      before { resolver.begin_scope }

      it 'sets the given +name.lexeme+ within +@@scopes+' do
        expect { subject }
          .to change { described_class.class_variable_get(:@@scopes) }
          .from([{}])
          .to([{ ';' => true }])
      end
    end
  end

  describe '#resolve_local' do
    subject { resolver.resolve_local(expr, token) }

    let(:resolver) { described_class.new(interpreter) }
    let(:interpreter) { Interpreter.new }
    let(:token) { Token.new('myVar', 1, 'nil', TokenTypes::IDENTIFIER) }
    let(:expr) { Expr::Variable.new(token) }

    context 'when the lexeme is in +@@scopes+' do
      before do
        described_class.scopes = [{ 'myVar' => false }]
      end

      it 'sets the given lexeme in the interpreter locals' do
        expect { subject }
          .to change { resolver.interpreter.class.locals }
          .from({}).to(expr => 0)
        expect(subject).to eq(nil)
      end
    end

    context 'when the lexeme is not in +@@scopes+' do
      it 'does not modify the interpreter locals' do
        expect { subject }
          .not_to change { resolver.interpreter.class.locals }
        expect(subject).to eq(nil)
      end
    end
  end

  describe '#resolve_function' do
    skip 'TBD'
    # see example 'multiple functions with same name in different scopes'
  end

  describe '#visit_l_class_stmt' do
    skip 'TBD' # To be updated.
  end
end
