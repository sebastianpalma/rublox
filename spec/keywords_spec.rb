# frozen_string_literal: true

require_relative './spec_helper'

require_relative './../lib/keywords'

RSpec.describe Keywords do
  it { expect(described_class::AND).to eq(:and) }
  it { expect(described_class::CLASS).to eq(:class) }
  it { expect(described_class::ELSE).to eq(:else) }
  it { expect(described_class::FALSE).to eq(:false) }
  it { expect(described_class::FOR).to eq(:for) }
  it { expect(described_class::FUN).to eq(:fun) }
  it { expect(described_class::IF).to eq(:if) }
  it { expect(described_class::NIL).to eq(:nil) }
  it { expect(described_class::OR).to eq(:or) }
  it { expect(described_class::PRINT).to eq(:print) }
  it { expect(described_class::RETURN).to eq(:return) }
  it { expect(described_class::SUPER).to eq(:super) }
  it { expect(described_class::THIS).to eq(:this) }
  it { expect(described_class::TRUE).to eq(:true) }
  it { expect(described_class::VAR).to eq(:var) }
  it { expect(described_class::WHILE).to eq(:while) }
end
