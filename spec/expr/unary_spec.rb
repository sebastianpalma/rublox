# frozen_string_literal: true

require_relative './../spec_helper'

require_relative './../../lib/expr/binary'
require_relative './../../lib/interpreter'
require_relative './../../lib/expr/literal'
require_relative './../../lib/token'
require_relative './../../lib/token_types'

RSpec.describe Expr::Binary do
  describe '#accept' do
    subject { described_class.new(left, operator, right).accept(visitor) }

    let(:left) { Expr::Literal.new(1.0) }
    let(:operator) { Token.new('+', 1, 'nil', TokenTypes::PLUS) }
    let(:right) { Expr::Literal.new(3.0) }
    let(:visitor) { Interpreter.new }

    it do
      expect(subject).to eq(4.0)
    end
  end
end
