# frozen_string_literal: true

require_relative './../spec_helper'

require_relative './../../lib/expr/literal'
require_relative './../../lib/expr/logical'
require_relative './../../lib/interpreter'
require_relative './../../lib/token'
require_relative './../../lib/token_types'

RSpec.describe Expr::Logical do
  describe '#accept' do
    subject { described_class.new(left, operator, right).accept(visitor) }

    let(:visitor) { Interpreter.new }

    describe 'TokenTypes::AND' do
      # @example
      #
      # 1.0 and nil;
      #
      context 'truthy left' do
        let(:left) { Expr::Literal.new(1.0) }
        let(:operator) { Token.new('and', 1, 'nil', TokenTypes::AND) }
        let(:right) { Expr::Literal.new(nil) }

        it 'returns right' do
          expect(subject).to eq(nil)
        end
      end

      # @example
      #
      # nil and "hej";
      #
      context 'truthy right' do
        let(:left) { Expr::Literal.new(nil) }
        let(:operator) { Token.new('and', 1, 'nil', TokenTypes::AND) }
        let(:right) { Expr::Literal.new('hej') }

        it 'returns left' do
          expect(subject).to eq(nil)
        end
      end
    end

    describe 'TokenTypes::OR' do
      # @example
      #
      # 1.0 or nil;
      #
      context 'truthy left' do
        let(:left) { Expr::Literal.new(1.0) }
        let(:operator) { Token.new('or', 1, 'nil', TokenTypes::OR) }
        let(:right) { Expr::Literal.new(nil) }

        it 'returns left' do
          expect(subject).to eq(1.0)
        end
      end

      # @example
      #
      # nil or "hej";
      #
      context 'truthy right' do
        let(:left) { Expr::Literal.new(nil) }
        let(:operator) { Token.new('or', 1, 'nil', TokenTypes::OR) }
        let(:right) { Expr::Literal.new('hej') }

        it 'returns right' do
          expect(subject).to eq('hej')
        end
      end
    end
  end
end
