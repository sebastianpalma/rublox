# frozen_string_literal: true

require_relative './../spec_helper'

require_relative './../../lib/interpreter'
require_relative './../../lib/token'
require_relative './../../lib/token_types'
require_relative './../../lib/expr/call'
require_relative './../../lib/expr/get'
require_relative './../../lib/expr/variable'

RSpec.describe Expr::Get do
  let(:object) do
    Expr::Call.new(
      Expr::Variable.new(
        Token.new('Bacon', 7, 'nil', TokenTypes::IDENTIFIER),
      ),
      Token.new(')', 1, 'nil', TokenTypes::RIGHT_PAREN),
      [],
    )
  end
  let(:name) { Token.new('eat', 7, 'nil', TokenTypes::IDENTIFIER) }

  describe '#accept' do
    subject { instance.accept(visitor) }

    let(:instance) { described_class.new(object, name) }
    let(:visitor) do
      Resolver.new(
        Interpreter.new,
      )
    end

    it 'implements visit_get_expr' do
      allow(visitor)
        .to receive(:visit_get_expr)
        .with(instance)

      subject

      expect(visitor)
        .to have_received(:visit_get_expr)
        .with(instance)
    end
  end

  describe '#==' do
    context 'when +other+ is not an Expr::Get instance' do
      subject do
        described_class
          .new(object, name)
          .==(
            Token.new(';', 1, 'nil', TokenTypes::SEMICOLON),
          )
      end

      it 'returns false' do
        expect(subject).to eq(false)
      end
    end

    context 'when +object+s are different' do
      subject do
        described_class
          .new(object, name)
          .==(
            described_class.new(
              Expr::Call.new(
                Expr::Variable.new(
                  Token.new('Hej', 1, 'nil', TokenTypes::IDENTIFIER),
                ),
                Token.new(';', 1, 'nil', TokenTypes::SEMICOLON),
                [],
              ),
              name,
            )
          )
      end

      it 'returns false' do
        expect(subject).to eq(false)
      end
    end

    context 'when +name+s are different' do
      subject do
        described_class
          .new(object, name)
          .==(
            described_class.new(
              object,
              Token.new('hej', 1, 'nil', TokenTypes::IDENTIFIER),
            )
          )
      end

      it 'returns false' do
        expect(subject).to eq(false)
      end
    end

    context 'when +object+s and +name+s are equal' do
      subject do
        described_class
          .new(object, name)
          .==(described_class.new(object, name))
      end

      it 'returns true' do
        expect(subject).to eq(true)
      end
    end
  end
end
