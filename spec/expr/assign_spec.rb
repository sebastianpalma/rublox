# frozen_string_literal: true

require_relative './../spec_helper'

require_relative './../../lib/expr/assign'
require_relative './../../lib/expr/literal'
require_relative './../../lib/interpreter'
require_relative './../../lib/token'
require_relative './../../lib/token_types'

RSpec.describe Expr::Assign do
  describe '#accept' do
    subject { described_class.new(name, value).accept(visitor) }

    let(:name) { Token.new('myVar', 1, 'nil', TokenTypes::IDENTIFIER) }
    let(:value) { Expr::Literal.new(3.0) }
    let(:visitor) do
      Interpreter.new.tap do |interpreter|
        interpreter.class.globals.class.values = { 'myVar' => 1.0 }
      end
    end

    it 'sets the value of the previously defined variable +myVar+ in the visitor instance' do
      subject
      expect(visitor.class.environment.class.values['myVar']).to eq(3.0)
    end

    it 'returns the given value' do
      expect(subject).to eq(3.0)
    end
  end
end
