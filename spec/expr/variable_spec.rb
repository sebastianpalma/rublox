# frozen_string_literal: true

require_relative './../spec_helper'

require_relative './../../lib/expr/variable'
require_relative './../../lib/interpreter'
require_relative './../../lib/token'
require_relative './../../lib/token_types'

RSpec.describe Expr::Variable do
  describe '#accept' do
    subject { described_class.new(name).accept(visitor) }

    let(:name) { Token.new('myVar', 1, 'nil', TokenTypes::IDENTIFIER) }
    let(:visitor) do
      Interpreter.new.tap do |interpreter|
        interpreter.class.globals.class.values = { 'myVar' => 91 }
      end
    end

    it { expect(subject).to eq(91) }
  end
end
