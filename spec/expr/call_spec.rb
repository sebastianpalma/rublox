# frozen_string_literal: true

require_relative './../spec_helper'

require_relative './../../lib/expr/call'
require_relative './../../lib/expr/literal'
require_relative './../../lib/expr/variable'
require_relative './../../lib/stmt/function'
require_relative './../../lib/stmt/print'
require_relative './../../lib/interpreter'
require_relative './../../lib/token'
require_relative './../../lib/token_types'
require_relative './../../lib/lox_function'

RSpec.describe Expr::Call do
  describe '#accept' do
    subject { described_class.new(callee, paren, arguments).accept(visitor) }

    let(:callee) do
      Expr::Variable.new(
        Token.new('fn', nil, '5', TokenTypes::IDENTIFIER),
      )
    end
    let(:paren) do
      Token.new(')', nil, '4', TokenTypes::RIGHT_PAREN)
    end
    let(:arguments) do
      []
    end
    let(:visitor) { Interpreter.new }

    before do
      visitor.class.environment.tap do |env|
        fn =
          Stmt::Function.new(
            Token.new('fn', nil, 1, TokenTypes::IDENTIFIER),
            [],
            [
              Stmt::Print.new(
                Expr::Literal.new('hej')
              )
            ],
          )
        env.define(fn.name.lexeme, LoxFunction.new(fn, env, nil))
      end
    end

    it 'delegates the call expr logic to the visitor' do
      expect { subject }.to output("hej\n").to_stdout
    end
  end
end
