# frozen_string_literal: true

require_relative './../spec_helper'

require_relative './../../lib/interpreter'
require_relative './../../lib/token'
require_relative './../../lib/token_types'
require_relative './../../lib/expr/call'
require_relative './../../lib/expr/set'
require_relative './../../lib/expr/variable'

RSpec.describe Expr::Set do
  skip 'TBD'
end
