# frozen_string_literal: true

require_relative './../spec_helper'

require_relative './../../lib/expr/grouping'
require_relative './../../lib/interpreter'
require_relative './../../lib/expr/literal'

RSpec.describe Expr::Grouping do
  describe '#accept' do
    subject { described_class.new(expression).accept(visitor) }

    let(:expression) { Expr::Literal.new(1.0) }
    let(:visitor) { Interpreter.new }

    it { expect(subject).to eq(1.0) }
  end
end
