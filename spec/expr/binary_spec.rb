# frozen_string_literal: true

require_relative './../spec_helper'

require_relative './../../lib/interpreter'
require_relative './../../lib/expr/literal'
require_relative './../../lib/token'
require_relative './../../lib/token_types'
require_relative './../../lib/expr/unary'

RSpec.describe Expr::Unary do
  describe '#accept' do
    subject { described_class.new(operator, right).accept(visitor) }

    let(:operator) { Token.new('!', 1, 'nil', TokenTypes::BANG) }
    let(:right) { Expr::Literal.new(true) }
    let(:visitor) { Interpreter.new }

    it { expect(subject).to eq(false) }
  end
end
