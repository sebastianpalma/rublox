# frozen_string_literal: true

require_relative './spec_helper'

require_relative './../lib/scanner'

RSpec.describe Scanner do
  describe '#scan_tokens' do
    subject { described_class.new(source).scan_tokens }

    let(:tokens) { subject[0] }
    let(:had_error) { subject[1] }
    let(:error) { subject[2] }

    context 'token types' do
      context '(' do
        let(:source) { '(' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq('(')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:left_paren)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context ')' do
        let(:source) { ')' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq(')')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:right_paren)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context '{' do
        let(:source) { '{' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq('{')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:left_brace)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context '}' do
        let(:source) { '}' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq('}')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:right_brace)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context ',' do
        let(:source) { ',' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq(',')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:comma)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context '.' do
        let(:source) { '.' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq('.')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:dot)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context '-' do
        let(:source) { '-' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq('-')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:minus)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context '+' do
        let(:source) { '+' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq('+')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:plus)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context ';' do
        let(:source) { ';' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq(';')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:semicolon)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context '*' do
        let(:source) { '*' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq('*')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:star)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context '!' do
        let(:source) { '!' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq('!')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:bang)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context '!=' do
        let(:source) { '!=' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq('!=')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:bang_equal)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context '=' do
        let(:source) { '=' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq('=')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:equal)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context '==' do
        let(:source) { '==' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq('==')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:equal_equal)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context '<' do
        let(:source) { '<' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq('<')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:less)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context '<=' do
        let(:source) { '<=' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq('<=')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:less_equal)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context '>' do
        let(:source) { '>' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq('>')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:greater)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context '>=' do
        let(:source) { '>=' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq('>=')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:greater_equal)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context '/' do
        context 'division /' do
          let(:source) { '/' }

          it do
            expect(tokens.size).to eq(2)

            expect(tokens[0].send(:lexeme)).to eq('/')
            expect(tokens[0].send(:line)).to eq(1)
            expect(tokens[0].send(:literal)).to eq('nil')
            expect(tokens[0].send(:type)).to eq(:slash)

            expect(tokens[1].send(:lexeme)).to eq('')
            expect(tokens[1].send(:line)).to eq(1)
            expect(tokens[1].send(:literal)).to eq('nil')
            expect(tokens[1].send(:type)).to eq(:eof)

            expect(had_error).to eq(false)

            expect(error).to eq('')
          end
        end

        context 'comment //' do
          let(:source) { '//' }

          it 'does not add a token' do
            expect(tokens.size).to eq(1)

            expect(tokens[0].send(:lexeme)).to eq('')
            expect(tokens[0].send(:line)).to eq(1)
            expect(tokens[0].send(:literal)).to eq('nil')
            expect(tokens[0].send(:type)).to eq(:eof)

            expect(had_error).to eq(false)

            expect(error).to eq('')
          end
        end
      end

      context ' ' do
        let(:source) { ' ' }

        it 'does not add a token' do
          expect(tokens.size).to eq(1)

          expect(tokens[0].send(:lexeme)).to eq('')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context "\r" do
        let(:source) { "\r" }

        it 'does not add a token' do
          expect(tokens.size).to eq(1)

          expect(tokens[0].send(:lexeme)).to eq('')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context "\t" do
        let(:source) { "\t" }

        it 'does not add a token' do
          expect(tokens.size).to eq(1)

          expect(tokens[0].send(:lexeme)).to eq('')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context "\n" do
        let(:source) { "\n" }

        it 'adds 1 to @line' do
          expect(tokens.size).to eq(1)

          expect(tokens[0].send(:lexeme)).to eq('')
          expect(tokens[0].send(:line)).to eq(2)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context '"' do
        context 'unterminated string - single double quote (")' do
          let(:source) { '"' }

          it do
            expect(tokens.size).to eq(1)

            expect(tokens[0].send(:lexeme)).to eq('')
            expect(tokens[0].send(:line)).to eq(1)
            expect(tokens[0].send(:literal)).to eq('nil')
            expect(tokens[0].send(:type)).to eq(:eof)

            expect(had_error).to eq(true)

            expect(error).to eq('Unterminated string.')
          end
        end

        context 'terminated string - two consecutive double quotes ("")' do
          let(:source) { '"hola"' }

          it do
            expect(tokens.size).to eq(2)

            expect(tokens[0].send(:lexeme)).to eq('"hola"')
            expect(tokens[0].send(:line)).to eq(1)
            expect(tokens[0].send(:literal)).to eq('hola')
            expect(tokens[0].send(:type)).to eq(:string)

            expect(tokens[1].send(:lexeme)).to eq('')
            expect(tokens[1].send(:line)).to eq(1)
            expect(tokens[1].send(:literal)).to eq('nil')
            expect(tokens[1].send(:type)).to eq(:eof)

            expect(had_error).to eq(false)

            expect(error).to eq('')
          end
        end
      end

      context 'digit' do
        context 'integer' do
          let(:source) { '123' }

          it do
            expect(tokens.size).to eq(2)

            expect(tokens[0].send(:lexeme)).to eq('123')
            expect(tokens[0].send(:line)).to eq(1)
            expect(tokens[0].send(:literal)).to eq(123.0)
            expect(tokens[0].send(:type)).to eq(:number)

            expect(tokens[1].send(:lexeme)).to eq('')
            expect(tokens[1].send(:line)).to eq(1)
            expect(tokens[1].send(:literal)).to eq('nil')
            expect(tokens[1].send(:type)).to eq(:eof)

            expect(had_error).to eq(false)

            expect(error).to eq('')
          end
        end

        context 'float' do
          let(:source) { '123.456' }

          it do
            expect(tokens.size).to eq(2)

            expect(tokens[0].send(:lexeme)).to eq('123.456')
            expect(tokens[0].send(:line)).to eq(1)
            expect(tokens[0].send(:literal)).to eq(123.456)
            expect(tokens[0].send(:type)).to eq(:number)

            expect(tokens[1].send(:lexeme)).to eq('')
            expect(tokens[1].send(:line)).to eq(1)
            expect(tokens[1].send(:literal)).to eq('nil')
            expect(tokens[1].send(:type)).to eq(:eof)

            expect(had_error).to eq(false)

            expect(error).to eq('')
          end
        end
      end

      context 'alphanumeric' do
        context '_' do
          let(:source) { '_var' }

          it do
            expect(tokens.size).to eq(2)

            expect(tokens[0].send(:lexeme)).to eq('_var')
            expect(tokens[0].send(:line)).to eq(1)
            expect(tokens[0].send(:literal)).to eq('nil')
            expect(tokens[0].send(:type)).to eq(:identifier)

            expect(tokens[1].send(:lexeme)).to eq('')
            expect(tokens[1].send(:line)).to eq(1)
            expect(tokens[1].send(:literal)).to eq('nil')
            expect(tokens[1].send(:type)).to eq(:eof)

            expect(had_error).to eq(false)

            expect(error).to eq('')
          end
        end

        context 'a-zA-Z0-9' do
          let(:source) { 'totalHeight1' }

          it do
            expect(tokens.size).to eq(2)

            expect(tokens[0].send(:lexeme)).to eq('totalHeight1')
            expect(tokens[0].send(:line)).to eq(1)
            expect(tokens[0].send(:literal)).to eq('nil')
            expect(tokens[0].send(:type)).to eq(:identifier)

            expect(tokens[1].send(:lexeme)).to eq('')
            expect(tokens[1].send(:line)).to eq(1)
            expect(tokens[1].send(:literal)).to eq('nil')
            expect(tokens[1].send(:type)).to eq(:eof)

            expect(had_error).to eq(false)

            expect(error).to eq('')
          end
        end

        context 'any other' do
          let(:source) { '#wrong' }

          it 'uses Lox.error to handle syntax errors' do
            expect(tokens.size).to eq(2)

            expect(tokens[0].send(:lexeme)).to eq('wrong')
            expect(tokens[0].send(:line)).to eq(1)
            expect(tokens[0].send(:literal)).to eq('nil')
            expect(tokens[0].send(:type)).to eq(:identifier)

            expect(tokens[1].send(:lexeme)).to eq('')
            expect(tokens[1].send(:line)).to eq(1)
            expect(tokens[1].send(:literal)).to eq('nil')
            expect(tokens[1].send(:type)).to eq(:eof)

            expect(had_error).to eq(true)

            expect(error).to eq('[line 1] Error : Unexpected character.')
          end
        end
      end
    end

    context 'keywords' do
      context 'and' do
        let(:source) { 'and' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq('and')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:and)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context 'class' do
        let(:source) { 'class' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq('class')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:class)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context 'else' do
        let(:source) { 'else' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq('else')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:else)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context 'false' do
        let(:source) { 'false' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq('false')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:false)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context 'for' do
        let(:source) { 'for' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq('for')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:for)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context 'fun' do
        let(:source) { 'fun' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq('fun')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:fun)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context 'if' do
        let(:source) { 'if' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq('if')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:if)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context 'nil' do
        let(:source) { 'nil' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq('nil')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:nil)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context 'or' do
        let(:source) { 'or' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq('or')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:or)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context 'print' do
        let(:source) { 'print' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq('print')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:print)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context 'return' do
        let(:source) { 'return' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq('return')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:return)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context 'super' do
        let(:source) { 'super' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq('super')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:super)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context 'this' do
        let(:source) { 'this' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq('this')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:this)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context 'true' do
        let(:source) { 'true' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq('true')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:true)


          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context 'var' do
        let(:source) { 'var' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq('var')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:var)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end

      context 'while' do
        let(:source) { 'while' }

        it do
          expect(tokens.size).to eq(2)

          expect(tokens[0].send(:lexeme)).to eq('while')
          expect(tokens[0].send(:line)).to eq(1)
          expect(tokens[0].send(:literal)).to eq('nil')
          expect(tokens[0].send(:type)).to eq(:while)

          expect(tokens[1].send(:lexeme)).to eq('')
          expect(tokens[1].send(:line)).to eq(1)
          expect(tokens[1].send(:literal)).to eq('nil')
          expect(tokens[1].send(:type)).to eq(:eof)

          expect(had_error).to eq(false)

          expect(error).to eq('')
        end
      end
    end
  end
end
