# frozen_string_literal: true

require_relative './spec_helper'

require_relative './../lib/return'

RSpec.describe Return do
  subject { described_class.new('hej') }

  it 'inherits from StandardError' do
    expect(described_class).to be < StandardError
  end

  it 'holds the given value' do
    expect(subject.value).to eq('hej')
  end
end
