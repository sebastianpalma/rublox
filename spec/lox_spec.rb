# frozen_string_literal: true

require_relative './spec_helper'

require_relative './../lib/lox'

RSpec.describe Lox do
  describe '.error' do
    subject { described_class.error(token, message) }

    context 'token.type == TokenTypes::EOF' do
      let(:token) { Token.new('', 1, 'nil', TokenTypes::EOF) }
      let(:message) { "Expect ';' after value." }

      it 'outputs to stdout a custom message' do
        expect { subject }
          .to output("[line 1] Error at end: Expect ';' after value.\n")
          .to_stdout
      end
    end

    context 'token.type != TokenTypes::EOF' do
      let(:token) { Token.new(';', 1, 'nil', TokenTypes::SEMICOLON) }
      let(:message) { "Expect expression." }

      it 'outputs to stdout a custom message' do
        expect { subject }
          .to output("[line 1] Error at ';': Expect expression.\n")
          .to_stdout
      end
    end
  end

  describe '.report' do
    subject { described_class.report(line, where, message) }

    let(:line) { 3 }
    let(:where) { " at '='" }
    let(:message) { 'Expect variable name.' }

    it 'outputs to stdout a custom message' do
      expect { subject }
        .to output("[line 3] Error at '=': Expect variable name.\n")
        .to_stdout
    end
  end

  describe '.runtime_error' do
    subject { described_class.runtime_error(error) }

    let(:error) do
      RuntimeError.new(
        "Undefined variable 'myVar'",
        Token.new('myVar', 1, 'nil', TokenTypes::IDENTIFIER)
      )
    end

    it 'outputs to stdout a custom message' do
      expect { subject }
        .to output("Undefined variable 'myVar'\n[line 1]\n")
        .to_stdout
    end
  end

  describe '#main' do
    subject { described_class.new([file]).main }

    context 'without syntax errors' do
      context 'single line file' do
        let(:file) { 'spec/support/files/singleline.lox' }

        it 'reads the file and outputs each token' do
          expect { subject }.to output("true\n").to_stdout
        end
      end

      context 'multi line file' do
        let(:file) { 'spec/support/files/multiline.lox' }

        it 'reads the file and outputs each token by line' do
          expect { subject }.to output("3\n").to_stdout
        end
      end
    end

    context 'with syntax errors' do
      let(:file) { 'spec/support/files/syntaxerror.lox' }

      it 'exits the program when encountered' do
        expect { subject }
          .to raise_error(SystemExit).and(
            output(
              "[line 2] Error at end: Expect ';' after expression.\n"
            ).to_stdout
          )
      end
    end
  end
end
