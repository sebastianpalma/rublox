# frozen_string_literal: true

require_relative './spec_helper'

require_relative './../lib/environment'
require_relative './../lib/lox_function'
require_relative './../lib/interpreter'
require_relative './../lib/token'
require_relative './../lib/token_types'
require_relative './../lib/stmt/block'
require_relative './../lib/stmt/if'
require_relative './../lib/stmt/function'
require_relative './../lib/stmt/print'
require_relative './../lib/stmt/var'
require_relative './../lib/stmt/expression'
require_relative './../lib/stmt/return'
require_relative './../lib/stmt/while'
require_relative './../lib/expr/assign'
require_relative './../lib/expr/binary'
require_relative './../lib/expr/literal'
require_relative './../lib/expr/variable'

RSpec.describe LoxFunction do
  describe '#to_s' do
    subject { described_class.new(declaration, closure, false).to_s }

    let(:closure) do
      Environment.new(nil)
    end
    let(:declaration) do
      Stmt::Function.new(
        Token.new('fn', nil, 1, TokenTypes::IDENTIFIER),
        [],
        [
          Stmt::Print.new(
            Expr::Literal.new(1.0),
          ),
        ],
      )
    end

    it { expect(subject).to eq("<fn fn>") }
  end

  describe '#arity' do
    let(:fn1) do
      described_class
        .new(
          Stmt::Function.new(
            Token.new('fn', nil, 1, TokenTypes::IDENTIFIER),
            # 0 arguments.
            [],
            [
              Stmt::Print.new(
                Expr::Literal.new(1.0)
              )
            ],
          ),
          Environment.new(nil),
          false,
        )
    end
    let(:fn2) do
      described_class
        .new(
          Stmt::Function.new(
            Token.new('fn', nil, 1, TokenTypes::IDENTIFIER),
            # 1 argument.
            [
              Token.new('g', nil, 1, TokenTypes::IDENTIFIER),
            ],
            [
              Stmt::Print.new(
                Expr::Literal.new(1.0)
              )
            ],
          ),
          Environment.new(nil),
          false,
        )
    end
    let(:fn3) do
      described_class
        .new(
          Stmt::Function.new(
            Token.new('fn', nil, 1, TokenTypes::IDENTIFIER),
            # 2 arguments.
            [
              Token.new('g', nil, 1, TokenTypes::IDENTIFIER),
              Token.new('h', nil, 1, TokenTypes::IDENTIFIER),
            ],
            [
              Stmt::Print.new(
                Expr::Literal.new(1.0)
              )
            ],
          ),
          Environment.new(nil),
          false,
        )
    end

    it 'returns the total number of arguments the function takes' do
      expect(fn1.arity).to eq(0)
      expect(fn2.arity).to eq(1)
      expect(fn3.arity).to eq(2)
    end
  end

  describe '#call' do
    context 'function without arguments' do
      subject do
        described_class
          .new(declaration, closure, false)
          .call(interpreter, arguments)
      end

      let(:closure) { Environment.new(nil) }
      let(:declaration) do
        Stmt::Function.new(
          Token.new('fn', nil, 1, TokenTypes::IDENTIFIER),
          [],
          [
            Stmt::Print.new(
              Expr::Literal.new("ay caramba")
            )
          ],
        )
      end
      let(:interpreter) do
        Interpreter.new.tap do |int|
          int.instance_variable_set(:@globals, closure)
          int.instance_variable_set(:@environment, closure)
        end
      end
      let(:arguments) { [] }

      before do
        # Define the given function in the LoxFunction closure.
        closure.define(
          declaration.name.lexeme,
          LoxFunction.new(declaration, closure, false)
        )
      end

      it 'executes the given function in its own environment' do
        expect { subject }.to output("ay caramba\n").to_stdout
      end
    end

    context 'function with arguments' do
      subject do
        described_class
          .new(declaration, closure, false)
          .call(interpreter, arguments)
      end

      let(:closure) { Environment.new(nil) }
      let(:declaration) do
        Stmt::Function.new(
          Token.new('fn', nil, 1, TokenTypes::IDENTIFIER),
          [
            Token.new('greet', nil, 1, TokenTypes::IDENTIFIER),
          ],
          [
            Stmt::Print.new(
              Expr::Variable.new(
                Token.new('greet', nil, 2, TokenTypes::IDENTIFIER),
              )
            )
          ],
        )
      end
      let(:interpreter) do
        Interpreter.new.tap do |int|
          int.instance_variable_set(:@globals, closure)
          int.instance_variable_set(:@environment, closure)
        end
      end
      let(:arguments) { ['hej'] }

      before do
        # Define the given function in the LoxFunction closure.
        closure.define(
          declaration.name.lexeme,
          LoxFunction.new(declaration, closure, false)
        )
      end

      it 'executes the given function in its own environment' do
        expect { subject }.to output("hej\n").to_stdout
      end
    end

    context 'function with explicit return' do
      subject do
        described_class
          .new(declaration, closure, false)
          .call(interpreter, arguments)
      end

      let(:closure) { Environment.new(nil) }
      let(:declaration) do
        # @example
        #
        # fun f() {
        #   var myVar = 1;
        #   while (true) {
        #     if (n == 1) {
        #       return "hej";
        #     }
        #   }
        # }
        # print f();
        # 
        Stmt::Function.new(
          Token.new('fn', nil, 1, TokenTypes::IDENTIFIER),
          [],
          [
            Stmt::Var.new(
              Token.new('myVar', nil, 1, TokenTypes::IDENTIFIER),
              Expr::Literal.new(1.0),
            ),
            Stmt::While.new(
              Expr::Literal.new(true),
              Stmt::Block.new(
                [
                  Stmt::If.new(
                    Expr::Binary.new(
                      Expr::Variable.new(
                        Token.new('myVar', nil, 1, TokenTypes::IDENTIFIER),
                      ),
                      Token.new('==', nil, 1, TokenTypes::EQUAL_EQUAL),
                      Expr::Literal.new(1.0),
                    ),
                    Stmt::Block.new(
                      [
                        Stmt::Return.new(
                          Token.new('return', nil, 1, TokenTypes::RETURN),
                          Expr::Literal.new('hej'),
                        ),
                      ],
                    ),
                    nil,
                  ),
                ],
              ),
            ),
          ],
        )
      end
      let(:interpreter) do
        Interpreter.new.tap do |int|
          int.instance_variable_set(:@globals, closure)
          int.instance_variable_set(:@environment, closure)
        end
      end
      let(:arguments) { [] }

      before do
        # Define the given function in the LoxFunction closure.
        closure.define(
          declaration.name.lexeme,
          LoxFunction.new(declaration, closure, false)
        )
      end

      it 'executes the given function in its own environment' do
        expect(subject).to eq('hej')
      end
    end
  end

  describe '#==' do
    let(:closure) do
      Environment.new(nil)
    end
    let(:declaration) do
      Stmt::Function.new(
        Token.new('fn', nil, 1, TokenTypes::IDENTIFIER),
        [],
        [
          Stmt::Print.new(
            Expr::Literal.new(1.0),
          ),
        ],
      )
    end

    context 'when +other+ is not a LoxFunction' do
      subject do
        described_class
          .new(declaration, closure, false)
          .==(Token.new(';', 1, 'nil', TokenTypes::IDENTIFIER))
      end

      it 'returns false' do
        expect(subject).to eq(false)
      end
    end

    context 'when +closure+s are different' do
      subject do
        described_class
          .new(declaration, closure, false)
          .==(
            described_class.new(
              declaration,
              Environment.new(Environment.new),
              false,
            ),
          )
      end

      it 'returns false' do
        expect(subject).to eq(false)
      end
    end

    context 'when +declarations+s are different' do
      subject do
        described_class
          .new(declaration, closure, false)
          .==(
            described_class.new(
              Stmt::Function.new(
                Token.new('fn', nil, 1, TokenTypes::IDENTIFIER),
                [],
                [
                  Stmt::Print.new(
                    Expr::Literal.new('hej'),
                  ),
                ],
              ),
              closure,
              false,
            ),
          )
      end

      it 'returns false' do
        expect(subject).to eq(false)
      end
    end

    context 'when +closure+s and +declarations+s are equal' do
      subject do
        described_class
          .new(declaration, closure, false)
          .==(described_class.new(declaration, closure, false))
      end

      it 'returns true' do
        expect(subject).to eq(true)
      end
    end
  end
end
