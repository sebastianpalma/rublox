# frozen_string_literal: true

require_relative './spec_helper'

require_relative './../lib/lox'
require_relative './../lib/parser'
require_relative './../lib/token'

RSpec.describe Parser do
  describe '#parse' do
    subject { described_class.new(tokens).parse }
    
    let(:statement) { subject[0] }

    context 'on parse error' do
      #
      # (1;
      #
      context 'missing a closing paren' do
        let(:tokens) do
          [
            Token.new('(', 1, 'nil', TokenTypes::LEFT_PAREN),
            Token.new('1', 1, 1.0,   TokenTypes::NUMBER),
            Token.new(';', 1, 'nil',   TokenTypes::SEMICOLON),
            Token.new('',  2, 'nil', TokenTypes::EOF),
          ]
        end

        it do
          expect { subject }.to output(
            "[line 1] Error at ';': Expect ')' after expression.\n"
          ).to_stdout
        end
      end

      #
      # -
      #
      context 'missing an identifier' do
        let(:tokens) do
          [
            Token.new('-', 1, 'nil', TokenTypes::MINUS),
            Token.new('',  2, 'nil', TokenTypes::EOF),
          ]
        end

        it do
          expect { subject }.to(
            output(
              "[line 2] Error at end: Expect expression.\n"
            ).to_stdout
          )
        end
      end

      #
      # print "hola"
      #
      context 'missing semicolon after Stmt::Print' do
        let(:tokens) do
          [
            Token.new('print',  9, 'nil', TokenTypes::PRINT),
            Token.new('"hola"', 9, 'nil', TokenTypes::STRING),
            Token.new('',       10, 'nil', TokenTypes::EOF),
          ]
        end

        it do
          expect { subject }.to(
            output(
              "[line 10] Error at end: Expect ';' after value.\n"
            ).to_stdout
          )
        end
      end

      #
      # var = 2;
      #
      context 'missing variable name after Stmt::Var' do
        let(:tokens) do
          [
            Token.new('var', 3, 'nil', TokenTypes::VAR),
            Token.new('=',   3, 'nil', TokenTypes::EQUAL),
            Token.new('2',   3, 2.0,   TokenTypes::NUMBER),
            Token.new(';',   3, 'nil', TokenTypes::SEMICOLON),
            Token.new('',    4, 'nil', TokenTypes::EOF),
          ]
        end

        it do
          expect { subject }.to(
            output(
              "[line 3] Error at '=': Expect variable name.\n"
            ).to_stdout
          )
        end
      end

      #
      # var myVar = 1
      #
      context 'missing variable name after declaring var' do
        let(:tokens) do
          [
            Token.new('var',   3, 'nil', TokenTypes::VAR),
            Token.new('myVar', 3, 'nil', TokenTypes::IDENTIFIER),
            Token.new('=',     3, 'nil', TokenTypes::EQUAL),
            Token.new('2',     3, 2.0,   TokenTypes::NUMBER),
            Token.new('',      4, 'nil', TokenTypes::EOF),
          ]
        end

        it do
          expect { subject }.to(
            output(
              "[line 4] Error at end: Expect ';' after variable declaration.\n"
            ).to_stdout
          )
        end
      end

      #
      # myVar = 3
      #
      context 'missing semicolon after Stmt::Var' do
        let(:tokens) do
          [
            Token.new('myVar', 2, 'nil', TokenTypes::IDENTIFIER),
            Token.new('=',     2, 'nil', TokenTypes::EQUAL),
            Token.new('3',     2, 3.0,   TokenTypes::NUMBER),
            Token.new('',      3, 'nil', TokenTypes::EOF),
          ]
        end

        it do
          expect { subject }.to(
            output(
              "[line 3] Error at end: Expect ';' after expression.\n"
            ).to_stdout
          )
        end
      end

      # 
      # (myVar) = 1;
      #
      context 'invalid target while assigning' do
        let(:tokens) do
          [
            Token.new('(',     1,   'nil', TokenTypes::LEFT_PAREN),
            Token.new('myVar', 1,   'nil', TokenTypes::IDENTIFIER),
            Token.new(')',     1,   3.0,   TokenTypes::RIGHT_PAREN),
            Token.new('=',     2,   'nil', TokenTypes::EQUAL),
            Token.new('1',     1.0, 'nil', TokenTypes::NUMBER),
            Token.new(';',     1,   'nil', TokenTypes::SEMICOLON),
            Token.new('',      2,   'nil', TokenTypes::EOF),
          ]
        end

        it do
          expect { subject }.to(
            output(
              "[line 2] Error at '=': Invalid assignment target.\n"
            ).to_stdout
          )
        end
      end
    end

    context 'TokenTypes::BANG_EQUAL' do
      context do
        let(:tokens) do
          [
            Token.new('1',  1, 1.0,   TokenTypes::NUMBER),
            Token.new('!=', 1, 'nil', TokenTypes::BANG_EQUAL),
            Token.new('2',  1, 2.0,   TokenTypes::NUMBER),
            Token.new(';',  1, 'nil', TokenTypes::SEMICOLON),
            Token.new('',   2, 'nil', TokenTypes::EOF),
          ]
        end

        it do
          expect(subject.size).to eq(1)

          expect(statement).to be_a(Stmt::Expression)
          expect(statement.expression).to be_a(Expr::Binary)

          expect(statement.expression.left).to be_a(Expr::Literal)
          expect(statement.expression.left.value).to eq(1.0)

          expect(statement.expression.operator).to be_a(Token)
          expect(statement.expression.operator.lexeme).to eq('!=')
          expect(statement.expression.operator.line).to eq(1)
          expect(statement.expression.operator.literal).to eq('nil')
          expect(statement.expression.operator.type).to eq(TokenTypes::BANG_EQUAL)

          expect(statement.expression.right).to be_a(Expr::Literal)
          expect(statement.expression.right.value).to eq(2.0)
        end
      end
    end

    context 'TokenTypes::EQUAL_EQUAL' do
      context do
        let(:tokens) do
          [
            Token.new('1',  1, 1.0,   TokenTypes::NUMBER),
            Token.new('==', 1, 'nil', TokenTypes::EQUAL_EQUAL),
            Token.new('1',  1, 1.0,   TokenTypes::NUMBER),
            Token.new(';',  1, 'nil', TokenTypes::SEMICOLON),
            Token.new('',   2, 'nil', TokenTypes::EOF),
          ]
        end

        it do
          expect(subject.size).to eq(1)

          expect(statement).to be_a(Stmt::Expression)
          expect(statement.expression).to be_a(Expr::Binary)

          expect(statement.expression.left).to be_a(Expr::Literal)
          expect(statement.expression.left.value).to eq(1.0)

          expect(statement.expression.operator).to be_a(Token)
          expect(statement.expression.operator.lexeme).to eq('==')
          expect(statement.expression.operator.line).to eq(1)
          expect(statement.expression.operator.literal).to eq('nil')
          expect(statement.expression.operator.type).to eq(TokenTypes::EQUAL_EQUAL)

          expect(statement.expression.right).to be_a(Expr::Literal)
          expect(statement.expression.right.value).to eq(1.0)
        end
      end
    end

    context 'TokenTypes::GREATER' do
      context do
        let(:tokens) do
          [
            Token.new('1',  1, 1.0,   TokenTypes::NUMBER),
            Token.new('>', 1, 'nil',  TokenTypes::GREATER),
            Token.new('1',  1, 1.0,   TokenTypes::NUMBER),
            Token.new(';',  1, 'nil', TokenTypes::SEMICOLON),
            Token.new('',   2, 'nil', TokenTypes::EOF),
          ]
        end

        it do
          expect(subject.size).to eq(1)

          expect(statement).to be_a(Stmt::Expression)
          expect(statement.expression).to be_a(Expr::Binary)

          expect(statement.expression.left).to be_a(Expr::Literal)
          expect(statement.expression.left.value).to eq(1.0)

          expect(statement.expression.operator).to be_a(Token)
          expect(statement.expression.operator.lexeme).to eq('>')
          expect(statement.expression.operator.line).to eq(1)
          expect(statement.expression.operator.literal).to eq('nil')
          expect(statement.expression.operator.type).to eq(TokenTypes::GREATER)

          expect(statement.expression.right).to be_a(Expr::Literal)
          expect(statement.expression.right.value).to eq(1.0)
        end
      end
    end

    context 'TokenTypes::GREATER_EQUAL' do
      context do
        let(:tokens) do
          [
            Token.new('1',  1, 1.0,   TokenTypes::NUMBER),
            Token.new('>=', 1, 'nil', TokenTypes::GREATER_EQUAL),
            Token.new('1',  1, 1.0,   TokenTypes::NUMBER),
            Token.new(';',  1, 'nil', TokenTypes::SEMICOLON),
            Token.new('',   2, 'nil', TokenTypes::EOF),
          ]
        end

        it do
          expect(subject.size).to eq(1)

          expect(statement).to be_a(Stmt::Expression)
          expect(statement.expression).to be_a(Expr::Binary)

          expect(statement.expression.left).to be_a(Expr::Literal)
          expect(statement.expression.left.value).to eq(1.0)

          expect(statement.expression.operator).to be_a(Token)
          expect(statement.expression.operator.lexeme).to eq('>=')
          expect(statement.expression.operator.line).to eq(1)
          expect(statement.expression.operator.literal).to eq('nil')
          expect(statement.expression.operator.type).to eq(TokenTypes::GREATER_EQUAL)

          expect(statement.expression.right).to be_a(Expr::Literal)
          expect(statement.expression.right.value).to eq(1.0)
        end
      end
    end

    context 'TokenTypes::GREATER' do
      context do
        let(:tokens) do
          [
            Token.new('1',  1, 1.0,   TokenTypes::NUMBER),
            Token.new('<', 1, 'nil',  TokenTypes::LESS),
            Token.new('1',  1, 1.0,   TokenTypes::NUMBER),
            Token.new(';',  1, 'nil', TokenTypes::SEMICOLON),
            Token.new('',   2, 'nil', TokenTypes::EOF),
          ]
        end

        it do
          expect(subject.size).to eq(1)

          expect(statement).to be_a(Stmt::Expression)
          expect(statement.expression).to be_a(Expr::Binary)

          expect(statement.expression.left).to be_a(Expr::Literal)
          expect(statement.expression.left.value).to eq(1.0)

          expect(statement.expression.operator).to be_a(Token)
          expect(statement.expression.operator.lexeme).to eq('<')
          expect(statement.expression.operator.line).to eq(1)
          expect(statement.expression.operator.literal).to eq('nil')
          expect(statement.expression.operator.type).to eq(TokenTypes::LESS)

          expect(statement.expression.right).to be_a(Expr::Literal)
          expect(statement.expression.right.value).to eq(1.0)
        end
      end
    end

    context 'TokenTypes::LESS_EQUAL' do
      context do
        let(:tokens) do
          [
            Token.new('1',  1, 1.0,   TokenTypes::NUMBER),
            Token.new('<=', 1, 'nil', TokenTypes::LESS_EQUAL),
            Token.new('1',  1, 1.0,   TokenTypes::NUMBER),
            Token.new(';',  1, 'nil', TokenTypes::SEMICOLON),
            Token.new('',   2, 'nil', TokenTypes::EOF),
          ]
        end

        it do
          expect(subject.size).to eq(1)

          expect(statement).to be_a(Stmt::Expression)
          expect(statement.expression).to be_a(Expr::Binary)

          expect(statement.expression.left).to be_a(Expr::Literal)
          expect(statement.expression.left.value).to eq(1.0)

          expect(statement.expression.operator).to be_a(Token)
          expect(statement.expression.operator.lexeme).to eq('<=')
          expect(statement.expression.operator.line).to eq(1)
          expect(statement.expression.operator.literal).to eq('nil')
          expect(statement.expression.operator.type).to eq(TokenTypes::LESS_EQUAL)

          expect(statement.expression.right).to be_a(Expr::Literal)
          expect(statement.expression.right.value).to eq(1.0)
        end
      end
    end

    context 'TokenTypes::MINUS' do
      context do
        let(:tokens) do
          [
            Token.new('1',  1, 1.0,   TokenTypes::NUMBER),
            Token.new('-', 1, 'nil',  TokenTypes::MINUS),
            Token.new('1',  1, 1.0,   TokenTypes::NUMBER),
            Token.new(';',  1, 'nil', TokenTypes::SEMICOLON),
            Token.new('',   2, 'nil', TokenTypes::EOF),
          ]
        end

        it do
          expect(subject.size).to eq(1)

          expect(statement).to be_a(Stmt::Expression)
          expect(statement.expression).to be_a(Expr::Binary)

          expect(statement.expression.left).to be_a(Expr::Literal)
          expect(statement.expression.left.value).to eq(1.0)

          expect(statement.expression.operator).to be_a(Token)
          expect(statement.expression.operator.lexeme).to eq('-')
          expect(statement.expression.operator.line).to eq(1)
          expect(statement.expression.operator.literal).to eq('nil')
          expect(statement.expression.operator.type).to eq(TokenTypes::MINUS)

          expect(statement.expression.right).to be_a(Expr::Literal)
          expect(statement.expression.right.value).to eq(1.0)
        end
      end
    end

    context 'TokenTypes::PLUS' do
      context do
        let(:tokens) do
          [
            Token.new('1',  1, 1.0,   TokenTypes::NUMBER),
            Token.new('+', 1, 'nil',  TokenTypes::PLUS),
            Token.new('1',  1, 1.0,   TokenTypes::NUMBER),
            Token.new(';',  1, 'nil', TokenTypes::SEMICOLON),
            Token.new('',   2, 'nil', TokenTypes::EOF),
          ]
        end

        it do
          expect(subject.size).to eq(1)

          expect(statement).to be_a(Stmt::Expression)
          expect(statement.expression).to be_a(Expr::Binary)

          expect(statement.expression.left).to be_a(Expr::Literal)
          expect(statement.expression.left.value).to eq(1.0)

          expect(statement.expression.operator).to be_a(Token)
          expect(statement.expression.operator.lexeme).to eq('+')
          expect(statement.expression.operator.line).to eq(1)
          expect(statement.expression.operator.literal).to eq('nil')
          expect(statement.expression.operator.type).to eq(TokenTypes::PLUS)

          expect(statement.expression.right).to be_a(Expr::Literal)
          expect(statement.expression.right.value).to eq(1.0)
        end
      end
    end

    context 'TokenTypes::SLASH' do
      context do
        let(:tokens) do
          [
            Token.new('1',  1, 1.0,   TokenTypes::NUMBER),
            Token.new('/', 1, 'nil',  TokenTypes::SLASH),
            Token.new('1',  1, 1.0,   TokenTypes::NUMBER),
            Token.new(';',  1, 'nil', TokenTypes::SEMICOLON),
            Token.new('',   2, 'nil', TokenTypes::EOF),
          ]
        end

        it do
          expect(subject.size).to eq(1)

          expect(statement).to be_a(Stmt::Expression)
          expect(statement.expression).to be_a(Expr::Binary)

          expect(statement.expression.left).to be_a(Expr::Literal)
          expect(statement.expression.left.value).to eq(1.0)

          expect(statement.expression.operator).to be_a(Token)
          expect(statement.expression.operator.lexeme).to eq('/')
          expect(statement.expression.operator.line).to eq(1)
          expect(statement.expression.operator.literal).to eq('nil')
          expect(statement.expression.operator.type).to eq(TokenTypes::SLASH)

          expect(statement.expression.right).to be_a(Expr::Literal)
          expect(statement.expression.right.value).to eq(1.0)
        end
      end
    end

    context 'TokenTypes::STAR' do
      context do
        let(:tokens) do
          [
            Token.new('1',  1, 1.0,   TokenTypes::NUMBER),
            Token.new('*', 1, 'nil',  TokenTypes::STAR),
            Token.new('1',  1, 1.0,   TokenTypes::NUMBER),
            Token.new(';',  1, 'nil', TokenTypes::SEMICOLON),
            Token.new('',   2, 'nil', TokenTypes::EOF),
          ]
        end

        it do
          expect(subject.size).to eq(1)

          expect(statement).to be_a(Stmt::Expression)
          expect(statement.expression).to be_a(Expr::Binary)

          expect(statement.expression.left).to be_a(Expr::Literal)
          expect(statement.expression.left.value).to eq(1.0)

          expect(statement.expression.operator).to be_a(Token)
          expect(statement.expression.operator.lexeme).to eq('*')
          expect(statement.expression.operator.line).to eq(1)
          expect(statement.expression.operator.literal).to eq('nil')
          expect(statement.expression.operator.type).to eq(TokenTypes::STAR)

          expect(statement.expression.right).to be_a(Expr::Literal)
          expect(statement.expression.right.value).to eq(1.0)
        end
      end
    end

    context 'TokenTypes::BANG' do
      context do
        let(:tokens) do
          [
            Token.new('!',    1, 'nil', TokenTypes::BANG),
            Token.new('true', 1, 'nil', TokenTypes::TRUE),
            Token.new(';',    1, 'nil', TokenTypes::SEMICOLON),
            Token.new('',     2, 'nil', TokenTypes::EOF),
          ]
        end

        it do
          expect(subject.size).to eq(1)

          expect(statement).to be_a(Stmt::Expression)
          expect(statement.expression).to be_a(Expr::Unary)

          expect(statement.expression).not_to respond_to(:left)

          expect(statement.expression.operator).to be_a(Token)
          expect(statement.expression.operator.lexeme).to eq('!')
          expect(statement.expression.operator.line).to eq(1)
          expect(statement.expression.operator.literal).to eq('nil')
          expect(statement.expression.operator.type).to eq(TokenTypes::BANG)

          expect(statement.expression.right).to be_a(Expr::Literal)
          expect(statement.expression.right.value).to eq(true)
        end
      end
    end

    context 'TokenTypes::MINUS' do
      context do
        let(:tokens) do
          [
            Token.new('-', 1, 'nil', TokenTypes::MINUS),
            Token.new('1', 1, 1.0,   TokenTypes::NUMBER),
            Token.new(';', 1, 'nil', TokenTypes::SEMICOLON),
            Token.new('',  1, 'nil', TokenTypes::EOF),
          ]
        end

        it do
          expect(subject.size).to eq(1)

          expect(statement).to be_a(Stmt::Expression)
          expect(statement.expression).to be_a(Expr::Unary)

          expect(statement.expression).not_to respond_to(:left)

          expect(statement.expression.operator).to be_a(Token)
          expect(statement.expression.operator.lexeme).to eq('-')
          expect(statement.expression.operator.line).to eq(1)
          expect(statement.expression.operator.literal).to eq('nil')
          expect(statement.expression.operator.type).to eq(TokenTypes::MINUS)

          expect(statement.expression.right).to be_a(Expr::Literal)
          expect(statement.expression.right.value).to eq(1.0)
        end
      end
    end
  end
end
