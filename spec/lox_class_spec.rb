# frozen_string_literal: true

require_relative './spec_helper'

require_relative './../lib/lox_class'
require_relative './../lib/token'
require_relative './../lib/token_types'
require_relative './../lib/stmt/function'
require_relative './../lib/stmt/print'
require_relative './../lib/expr/literal'

RSpec.describe LoxClass do
  describe '#to_s' do
    subject do
      described_class
        .new(
          Token.new(';', nil, 1, TokenTypes::SEMICOLON),
          nil,
          {},
        )
        .to_s
    end

    it 'returns the name' do
      expect(subject)
        .to eq(Token.new(';', nil, 1, TokenTypes::SEMICOLON))
    end
  end

  describe '#==' do
    context 'when +other+ is not a LoxClass instance' do
      subject do
        described_class
          .new(
            Token.new(';', nil, 1, TokenTypes::SEMICOLON),
            nil,
            {},
          )
          .==(Token.new(')', nil, 1, TokenTypes::RIGHT_PAREN))
      end

      it 'returns false' do
        expect(subject).to eq(false)
      end
    end

    context 'when +name+s are different' do
      subject do
        described_class
          .new(
            Token.new(';', nil, 1, TokenTypes::SEMICOLON),
            nil,
            {},
          )
          .==(
            described_class.new(
              Token.new('myVar', nil, 1, TokenTypes::IDENTIFIER),
              nil,
              {},
            ),
          )
      end

      it 'returns false' do
        expect(subject).to eq(false)
      end
    end

    context 'when +methods+s are different' do
      subject do
        described_class
          .new(
            Token.new(';', nil, 1, TokenTypes::SEMICOLON),
            nil,
            {
              'hej' => LoxFunction.new(
                Stmt::Function.new(
                  Token.new('hej', 1, 'nil', TokenTypes::IDENTIFIER),
                  [],
                  [
                    Stmt::Print.new(
                      Expr::Literal.new('hej'),
                    ),
                  ],
                ),
                Environment.new.tap do |env|
                  env.values = { 'Bacon' => nil }
                end,
                false,
              ),
            },
          )
          .==(
            described_class
            .new(
              Token.new(';', nil, 1, TokenTypes::SEMICOLON),
              nil,
              {},
            ),
          )
      end

      it 'returns false' do
        expect(subject).to eq(false)
      end
    end

    context 'when +name+s and +methods+s are equal' do
      let(:name) do
        Token.new(';', nil, 1, TokenTypes::SEMICOLON)
      end
      let(:methods) do
        {
          'hej' => LoxFunction.new(
            Stmt::Function.new(
              Token.new('hej', 1, 'nil', TokenTypes::IDENTIFIER),
              [],
              [
                Stmt::Print.new(
                  Expr::Literal.new('hej'),
                ),
              ],
            ),
            Environment.new.tap do |env|
              env.values = { 'Bacon' => nil }
            end,
            false,
          ),
        }
      end

      subject do
        described_class
          .new(name, nil, methods)
          .==(described_class.new(name, nil, methods))
      end

      it 'returns true' do
        expect(subject).to eq(true)
      end
    end
  end

  describe '#find_method' do
    context 'when +methods+ contains the given method' do
      subject do
        described_class
          .new(
            Token.new('hej', 1, 'nil', TokenTypes::IDENTIFIER),
            nil,
            { 'hej' => method },
          )
          .find_method('hej')
      end

      let(:method) do
        LoxFunction.new(
          Stmt::Function.new(
            Token.new('hej', 1, 'nil', TokenTypes::IDENTIFIER),
            [],
            [
              Stmt::Print.new(
                Expr::Literal.new('hej'),
              ),
            ],
          ),
          Environment.new.tap do |env|
            env.values = { 'Bacon' => nil }
          end,
          false,
        )
      end

      it 'finds it and returns it' do
        expect(subject).to eq(method)
      end
    end

    context 'when +methods+ does not contain the given method' do
      subject do
        described_class
          .new(
            Token.new('hej', 1, 'nil', TokenTypes::IDENTIFIER),
            nil,
            {},
          )
          .find_method('hej')
      end

      it 'returns nil' do
        expect(subject).to eq(nil)
      end
    end
  end
end
