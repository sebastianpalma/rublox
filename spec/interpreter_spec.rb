# frozen_string_literal: true

require_relative './spec_helper'

require_relative './../lib/expr/assign'
require_relative './../lib/expr/binary'
require_relative './../lib/expr/call'
require_relative './../lib/expr/grouping'
require_relative './../lib/expr/literal'
require_relative './../lib/expr/logical'
require_relative './../lib/expr/unary'
require_relative './../lib/expr/variable'
require_relative './../lib/interpreter'
require_relative './../lib/lox'
require_relative './../lib/stmt/l_class'
require_relative './../lib/stmt/expression'
require_relative './../lib/stmt/function'
require_relative './../lib/stmt/var'
require_relative './../lib/token'
require_relative './../lib/token_types'

RSpec.describe Interpreter do
  let(:interpreter) { described_class.new }

  describe '#interpret' do
    subject { interpreter.interpret(statements) }

    context 'no RuntimeError' do
      let(:statements) do
        [
          Stmt::Var.new(
            Token.new('myVar', 1, 'nil', TokenTypes::IDENTIFIER),
            Expr::Literal.new(92.1),
          )
        ]
      end

      it 'executes every statement' do
        expect { subject }
          .to change { described_class.environment.class.values }
          .from({})
          .to('myVar' => 92.1)
      end
    end

    # @example
    #
    # myVar = "foo"; # When myVar has not been declared before.
    #
    context 'RuntimeError' do
      let(:statements) do
        [
          Stmt::Expression.new(
            Expr::Assign.new(
              Token.new('myVar', 1, 'nil', TokenTypes::IDENTIFIER),
              Expr::Literal.new(3.1),
            ),
          )
        ]
      end

      it 'executes every statement' do
        expect { subject }
          .to output("Undefined variable 'myVar'.\n[line 1]\n").to_stdout
      end
    end
  end

  # @example
  #
  # var myVar = 1;
  # myVar = 2;
  #
  describe '#visit_assign_expr' do
    subject { interpreter.visit_assign_expr(expr) }

    let(:expr) do
      Expr::Assign.new(
        Token.new('myVar', 2, 'nil', TokenTypes::IDENTIFIER),
        Expr::Literal.new(2.0),
      )
    end

    before do
      described_class.environment.class.values = { 'myVar' => 1.0 }
    end

    it 're-assigns the value of a previously-defined variable' do
      expect { subject }
        .to change { described_class.environment.class.values }
        .from('myVar' => 1.0)
        .to('myVar' => 2.0)
    end
  end

  describe '#visit_binary_expr' do
    subject { described_class.new.visit_binary_expr(expr) }

    let(:expr) { Expr::Binary.new(left, op, right) }

    context 'TokenTypes::GREATER' do
      let(:op) { Token.new('>', 1, 'nil', TokenTypes::GREATER) }

      context 'a > b' do
        let(:left) { Expr::Literal.new(2.0) }
        let(:right) { Expr::Literal.new(1.0) }

        it { expect(subject).to eq(true) }
      end

      context 'a < b' do
        let(:left) { Expr::Literal.new(1.0) }
        let(:right) { Expr::Literal.new(2.0) }

        it { expect(subject).to eq(false) }
      end

      context 'a == b' do
        let(:left) { Expr::Literal.new(1.0) }
        let(:right) { Expr::Literal.new(1.0) }

        it { expect(subject).to eq(false) }
      end

      context 'one of the values is not a float' do
        let(:left) { Expr::Literal.new(1.0) }
        let(:right) { Expr::Literal.new(nil) }

        it do
          expect { subject }
            .to raise_error(RuntimeError, 'Operands must be numbers')
        end
      end
    end

    context 'TokenTypes::GREATER_EQUAL' do
      let(:op) { Token.new('>=', 1, 'nil', TokenTypes::GREATER_EQUAL) }

      context 'a > b' do
        let(:left) { Expr::Literal.new(2.0) }
        let(:right) { Expr::Literal.new(1.0) }

        it { expect(subject).to eq(true) }
      end

      context 'a < b' do
        let(:left) { Expr::Literal.new(1.0) }
        let(:right) { Expr::Literal.new(2.0) }

        it { expect(subject).to eq(false) }
      end

      context 'a == b' do
        let(:left) { Expr::Literal.new(1.0) }
        let(:right) { left }

        it { expect(subject).to eq(true) }
      end

      context 'any of the values is not a float' do
        let(:left) { Expr::Literal.new(1.0) }
        let(:right) { Expr::Literal.new(nil) }

        it 'returns the sum of both numbers' do
          expect { subject }
            .to raise_error(RuntimeError, 'Operands must be numbers')
        end
      end
    end

    context 'TokenTypes::LESS' do
      let(:op) { Token.new('<', 1, 'nil', TokenTypes::LESS) }

      context 'a < b' do
        let(:left) { Expr::Literal.new(1.0) }
        let(:right) { Expr::Literal.new(2.0) }

        it { expect(subject).to eq(true) }
      end

      context 'a > b' do
        let(:left) { Expr::Literal.new(2.0) }
        let(:right) { Expr::Literal.new(1.0) }

        it { expect(subject).to eq(false) }
      end

      context 'a == b' do
        let(:left) { Expr::Literal.new(1.0) }
        let(:right) { Expr::Literal.new(1.0) }

        it { expect(subject).to eq(false) }
      end

      context 'one of the values is not a float' do
        let(:left) { Expr::Literal.new(nil) }
        let(:right) { Expr::Literal.new(1.0) }

        it do
          expect { subject }
            .to raise_error(RuntimeError, 'Operands must be numbers')
        end
      end
    end

    context 'TokenTypes::LESS_EQUAL' do
      let(:op) { Token.new('>=', 1, 'nil', TokenTypes::LESS_EQUAL) }

      context 'a < b' do
        let(:left) { Expr::Literal.new(1.0) }
        let(:right) { Expr::Literal.new(2.0) }

        it { expect(subject).to eq(true) }
      end

      context 'a > b' do
        let(:left) { Expr::Literal.new(2.0) }
        let(:right) { Expr::Literal.new(1.0) }

        it { expect(subject).to eq(false) }
      end

      context 'a == b' do
        let(:left) { Expr::Literal.new(1.0) }
        let(:right) { left }

        it { expect(subject).to eq(true) }
      end

      context 'any of the values is not a float' do
        let(:left) { Expr::Literal.new(1.0) }
        let(:right) { Expr::Literal.new(nil) }

        it do
          expect { subject }
            .to raise_error(RuntimeError, 'Operands must be numbers')
        end
      end
    end

    context 'TokenTypes::MINUS' do
      let(:op) { Token.new('-', 1, 'nil', TokenTypes::MINUS) }

      context 'any of the values is neither float nor string' do
        let(:left) { Expr::Literal.new(nil) }
        let(:right) { Expr::Literal.new(1.0) }

        it do
          expect { subject }
            .to raise_error(RuntimeError, 'Operands must be numbers')
        end
      end

      context 'left and right are float' do
        let(:left) { Expr::Literal.new(5.0) }
        let(:right) { Expr::Literal.new(5.0) }

        it { expect(subject).to eq(0.0) }
      end
    end

    context 'TokenTypes::PLUS' do
      let(:op) { Token.new('+', 1, 'nil', TokenTypes::PLUS) }

      context 'any of the values is neither float nor string' do
        let(:left) { Expr::Literal.new(nil) }
        let(:right) { Expr::Literal.new(1.0) }

        it 'returns the sum of both numbers' do
          expect { subject }
            .to raise_error(
              RuntimeError,
              'Operands must be two numbers or two strings'
            )
        end
      end

      context 'left and right are float' do
        let(:left) { Expr::Literal.new(5.0) }
        let(:right) { Expr::Literal.new(5.0) }

        it 'returns the sum of both numbers' do
          expect(subject).to eq(10.0)
        end
      end

      context 'left and right are string' do
        let(:left) { Expr::Literal.new('abc') }
        let(:right) { Expr::Literal.new('xyz') }

        it 'returns the sum of both numbers' do
          expect(subject).to eq('abcxyz')
        end
      end
    end

    context 'TokenTypes::SLASH' do
      let(:op) { Token.new('/', 1, 'nil', TokenTypes::SLASH) }

      context 'any of the values is neither float nor string' do
        let(:left) { Expr::Literal.new(nil) }
        let(:right) { Expr::Literal.new(1.0) }

        it 'returns the sum of both numbers' do
          expect { subject }
            .to raise_error(RuntimeError, 'Operands must be numbers')
        end
      end

      context 'a divisible by b' do
        let(:left) { Expr::Literal.new(1.0) }
        let(:right) { Expr::Literal.new(1.0) }

        it { expect(subject).to eq(1.0) }
      end

      context 'a indivisible by b' do
        let(:left) { Expr::Literal.new(1.0) }
        let(:right) { Expr::Literal.new(0.0) }

        it { expect(subject).to eq(Float::INFINITY) }
      end
    end

    context 'TokenTypes::STAR' do
      let(:op) { Token.new('*', 1, 'nil', TokenTypes::STAR) }

      context 'any of the values is neither float nor string' do
        let(:left) { Expr::Literal.new(nil) }
        let(:right) { Expr::Literal.new(1.0) }

        it do
          expect { subject }
            .to raise_error(RuntimeError, 'Operands must be numbers')
        end
      end

      context 'left and right are float' do
        let(:left) { Expr::Literal.new(5.0) }
        let(:right) { Expr::Literal.new(5.0) }

        it { expect(subject).to eq(25.0) }
      end
    end

    context 'TokenTypes::BANG_EQUAL' do
      let(:op) { Token.new('!=', 1, 'nil', TokenTypes::BANG_EQUAL) }

      context 'left and right are equal' do
        let(:left) { Expr::Literal.new('a') }
        let(:right) { Expr::Literal.new('a') }

        it { expect(subject).to eq(false) }
      end

      context 'left and right are different' do
        let(:left) { Expr::Literal.new(1.0) }
        let(:right) { Expr::Literal.new(0.0) }

        it { expect(subject).to eq(true) }
      end
    end

    context 'TokenTypes::EQUAL_EQUAL' do
      let(:op) { Token.new('==', 1, 'nil', TokenTypes::EQUAL_EQUAL) }

      context 'left and right are equal' do
        let(:left) { Expr::Literal.new('a') }
        let(:right) { Expr::Literal.new('a') }

        it { expect(subject).to eq(true) }
      end

      context 'left and right are different' do
        let(:left) { Expr::Literal.new(1.0) }
        let(:right) { Expr::Literal.new(0.0) }

        it { expect(subject).to eq(false) }
      end
    end
  end

  describe '#visit_call_expr' do
    subject { interpreter.visit_call_expr(expr) }

    let(:interpreter) { described_class.new }

    context 'defined function' do
      context 'function without arguments' do
        let(:fn_id) { Token.new('fn', nil, 1, TokenTypes::IDENTIFIER) }
        let(:expr) do
          Expr::Call.new(
            Expr::Variable.new(fn_id),
            Token.new(')', nil, 1, TokenTypes::RIGHT_PAREN),
            [],
          )
        end

        before do
          described_class.environment.tap do |env|
            fn =
              Stmt::Function.new(
                fn_id,
                [],
                [
                  Stmt::Print.new(
                    Expr::Literal.new('hej')
                  ),
                ],
              )
            env.define(
              fn.name.lexeme,
              LoxFunction.new(fn, env, false)
            )
          end
        end

        it 'executes the given function' do
          expect { subject }.to output("hej\n").to_stdout
        end

        include_context 'suppressing output' do
          it 'returns nil' do
            expect(subject).to eq(nil)
          end
        end
      end

      context 'function with arguments' do
        context 'calling with the right amount of needed arguments' do
          let(:fn_id) { Token.new('fn', nil, 1, TokenTypes::IDENTIFIER) }
          let(:expr) do
            Expr::Call.new(
              Expr::Variable.new(fn_id),
              Token.new(')', nil, 1, TokenTypes::RIGHT_PAREN),
              [
                Expr::Literal.new('hej'),
              ],
            )
          end

          before do
            described_class.environment.tap do |env|
              fn =
                Stmt::Function.new(
                  fn_id,
                  [
                    Token.new('greet', nil, 1, TokenTypes::IDENTIFIER),
                  ],
                  [
                    Stmt::Print.new(
                      Expr::Variable.new(
                        Token.new('greet', nil, 2, TokenTypes::IDENTIFIER),
                      )
                    )
                  ],
                )
              env.define(fn.name.lexeme, LoxFunction.new(fn, env, false))
            end
          end

          it 'executes the given function' do
            expect { subject }.to output("hej\n").to_stdout
          end

          include_context 'suppressing output' do
            it 'returns nil' do
              expect(subject).to eq(nil)
            end
          end
        end
      end

      context 'calling without the right amount of needed arguments' do
        let(:fn_id) { Token.new('fn', nil, 1, TokenTypes::IDENTIFIER) }
        let(:expr) do
          Expr::Call.new(
            Expr::Variable.new(fn_id),
            Token.new(')', nil, 1, TokenTypes::RIGHT_PAREN),
            [],
          )
        end

        before do
          described_class.environment.tap do |env|
            fn =
              Stmt::Function.new(
                fn_id,
                [
                  Token.new('greet', nil, 1, TokenTypes::IDENTIFIER),
                ],
                [
                  Stmt::Print.new(
                    Expr::Variable.new(
                      Token.new('greet', nil, 2, TokenTypes::IDENTIFIER),
                    )
                  )
                ],
              )
            env.define(fn.name.lexeme, LoxFunction.new(fn, env, false))
          end
        end

        it 'raises an exception showing the number of arguments do not match' do
          expect { subject }
            .to raise_error(StandardError, 'Expected 1 arguments but got 0.')
        end
      end
    end

    context 'undefined function' do
      let(:fn_id) { Token.new('fn', nil, 1, TokenTypes::IDENTIFIER) }
      let(:expr) do
        Expr::Call.new(
          Expr::Variable.new(fn_id),
          Token.new(')', nil, 1, TokenTypes::RIGHT_PAREN),
          [],
        )
      end

      it 'raises an exception showing the number of arguments do not match' do
        expect { subject }
          .to raise_error(StandardError, "Undefined variable 'fn'.")
      end
    end

    context 'trying to call a non-function identifier' do
      let(:fn_id) { Token.new('f', nil, 1, TokenTypes::IDENTIFIER) }
      let(:expr) do
        Expr::Call.new(
          Expr::Variable.new(fn_id),
          Token.new(')', nil, 1, TokenTypes::RIGHT_PAREN),
          [],
        )
      end

      before do
        described_class.environment.tap do |env|
          # Define +f+ as a local variable. Then attempt to "call" it.
          env.define('f', 1.0)
        end
      end

      it 'raises an exception showing the number of arguments do not match' do
        expect { subject }
          .to raise_error(StandardError, 'Can only call functions and classes.')
      end
    end
  end

  describe '#visit_grouping_expr' do
    subject { described_class.new.visit_grouping_expr(expr) }

    let(:expr) { Expr::Grouping.new(Expr::Literal.new(1.0)) }

    it 'evaluates the given expression' do
      expect(subject).to eq(1.0)
    end
  end

  describe '#visit_get_expr' do
    skip 'TBD'
  end

  describe '#visit_set_expr' do
    skip 'TBD'
  end

  describe '#visit_literal_expr' do
    subject { described_class.new.visit_literal_expr(expr) }

    let(:expr) { Expr::Literal.new(7.0) }

    it 'returns the expression value' do
      expect(subject).to eq(7.0)
    end
  end

  describe '#visit_logical_expr' do
    subject { described_class.new.visit_logical_expr(expr) }

    context 'TokenTypes::AND' do
      # @example
      #
      # nil and true;
      #
      let(:expr) do
        Expr::Logical.new(
          Expr::Literal.new(nil),
          Token.new('and', 1, 'nil', TokenTypes::AND),
          Expr::Literal.new(true),
        )
      end

      it 'evaluates the expression and returns it' do
        expect(subject).to eq(nil)
      end
    end

    context 'TokenTypes::OR' do
      # @example
      #
      # true or true;
      #
      let(:expr) do
        Expr::Logical.new(
          Expr::Literal.new(true),
          Token.new('or', 1, 'nil', TokenTypes::OR),
          Expr::Literal.new(true),
        )
      end

      it 'evaluates the expression and returns it' do
        expect(subject).to eq(true)
      end
    end
  end

  describe '#visit_unary_expr' do
    subject { described_class.new.visit_unary_expr(expr) }

    context 'TokenTypes::BANG' do
      let(:expr) do
        Expr::Unary.new(
          Token.new('!', 1, 'nil', TokenTypes::BANG),
          Expr::Literal.new(true),
        )
      end

      it 'returns the expression value' do
        expect(subject).to eq(false)
      end
    end

    context 'TokenTypes::MINUS' do
      let(:expr) do
        Expr::Unary.new(
          Token.new('-', 1, 'nil', TokenTypes::MINUS),
          right
        )
      end

      context 'when +right+ is not a float' do
        let(:right) { Expr::Literal.new(true) }

        it do
          expect { subject }.to raise_error(
            RuntimeError,
            'The operand must be a number'
          )
        end
      end

      context 'when +right+ is a float' do
        let(:right) { Expr::Literal.new(12.5) }

        it { expect(subject).to eq(-12.5) }
      end
    end
  end

  # @example
  #
  # var myVar = 1;
  # myVar
  #
  describe '#visit_variable_expr' do
    subject { interpreter.visit_variable_expr(expr) }

    let(:expr) do
      Expr::Variable.new(
        Token.new('myVar', 1, 'nil', TokenTypes::IDENTIFIER)
      )
    end

    before do
      described_class.environment.class.values = { 'myVar' => 3 }
    end

    it 'returns the expression value' do
      expect(subject).to eq(3)
    end
  end

  #
  # Statements

  # @example
  #
  # { print "hej"; }
  #
  describe '#visit_block_stmt' do
    subject { interpreter.visit_block_stmt(stmt) }

    let(:stmt) do
      Stmt::Block.new(
        [
          Stmt::Print.new(
            Expr::Literal.new('hej'),
          )
        ]
      )
    end

    it 'evaluates the block in its own environment' do
      expect { subject }
        .to output("hej\n").to_stdout
    end

    # @example
    #
    # {
    #   {
    #     print "nested";
    #   }
    # }
    #
    context 'nested blocks' do
      let(:stmt) do
        Stmt::Block.new(
          [
            Stmt::Block.new(
              [
                Stmt::Print.new(
                  Expr::Literal.new('nested'),
                )
              ]
            )
          ]
        )
      end

      it 'evaluates the block in its own environment' do
        expect { subject }
          .to output("nested\n").to_stdout
      end
    end
  end

  # @example
  #
  # class Bacon {
  #   eat() {
  #     print "hej";
  #   }
  #
  #   greet() {
  #     print "hej";
  #   }
  # }
  #
  describe '#visit_l_class_stmt' do
    subject { interpreter.visit_l_class_stmt(stmt) }

    let(:stmt) do
      Stmt::LClass.new(
        Token.new('Bacon', 1, 'nil', TokenTypes::IDENTIFIER),
        nil,
        [
          Stmt::Function.new(
            Token.new('eat', 2, 'nil', TokenTypes::IDENTIFIER),
            [],
            [
              Stmt::Print.new(
                Expr::Literal.new('hej'),
              ),
            ],
          ),
          Stmt::Function.new(
            Token.new('greet', 6, 'nil', TokenTypes::IDENTIFIER),
            [],
            [
              Stmt::Print.new(
                Expr::Literal.new('hej'),
              ),
            ],
          ),
        ],
      )
    end

    it 'assigns the given class and its methods within the environment' do
      expect { subject }
        .to change { interpreter.environment.values }
        .from({})
        .to(
          'Bacon' => LoxClass.new(
            'Bacon',
            nil,
            {
              'eat' => LoxFunction.new(
                Stmt::Function.new(
                  Token.new('eat', 2, 'nil', TokenTypes::IDENTIFIER),
                  [],
                  [
                    Stmt::Print.new(
                      Expr::Literal.new('hej'),
                    ),
                  ],
                ),
                interpreter.environment,
                false,
              ),
              'greet' => LoxFunction.new(
                Stmt::Function.new(
                  Token.new('greet', 6, 'nil', TokenTypes::IDENTIFIER),
                  [],
                  [
                    Stmt::Print.new(
                      Expr::Literal.new('hej'),
                    ),
                  ],
                ),
                interpreter.environment,
                false,
              ),
            },
          )
        )
    end
  end

  # @example
  #
  # myVar = 1; # after successfully defining myVar previously
  #
  describe '#visit_expression_stmt' do
    subject { interpreter.visit_expression_stmt(stmt) }

    let(:stmt) do
      Stmt::Expression.new(
        Expr::Assign.new(
          Token.new('myVar', 2, 'nil', TokenTypes::IDENTIFIER),
          Expr::Literal.new(2.0),
        )
      )
    end

    before do
      described_class.environment.class.values = { 'myVar' => 1.0 }
    end

    it 'evaluates the statement expression' do
      expect { subject }
        .to change { described_class.environment.class.values }
        .from('myVar' => 1.0)
        .to('myVar' => 2.0)
    end

    it 'returns nil' do
      expect(subject).to eq(nil)
    end
  end

  # @example
  #
  # if (true) {
  #   print "hej";
  # }
  #
  describe '#visit_if_stmt' do
    subject { interpreter.visit_if_stmt(stmt) }

    let(:stmt) do
      Stmt::If.new(
        Expr::Literal.new(true),
        Stmt::Block.new([
          Stmt::Print.new(
            Expr::Literal.new("hej"),
          ),
        ]),
        nil,
      )
    end

    it 'evaluates the expression and outputs it to stdout' do
      expect { subject }
        .to output("hej\n").to_stdout
    end
  end

  # @example
  #
  # class Foo {
  # }
  #
  describe '#visit_l_class_stmt' do
    subject { interpreter.visit_l_class_stmt(stmt) }

    let(:stmt) do
      Stmt::LClass.new(
        Token.new('Foo', nil, 1, TokenTypes::CLASS),
        nil,
        [],
      )
    end

    it 'instantiates the class and assigns it to the environment' do
      expect { subject }
        .to change { interpreter.environment.values }
        .from({})
        .to('Foo' => LoxClass.new('Foo', nil, {}))
    end

    it 'returns nil' do
      expect(subject).to eq(nil)
    end
  end

  # @example
  #
  # print "hej";
  #
  describe '#visit_print_stmt' do
    subject { interpreter.visit_print_stmt(stmt) }

    let(:stmt) do
      Stmt::Print.new(
        Expr::Literal.new("hej")
      )
    end

    it 'evaluates the expression and outputs it to stdout' do
      expect { subject }
        .to output("hej\n").to_stdout
    end

    include_context 'suppressing output' do
      it 'returns nil' do
        expect(subject).to eq(nil)
      end
    end
  end

  describe '#visit_var_stmt' do
    subject { interpreter.visit_var_stmt(stmt) }

    context 'when stmt.initializer is Expr' do
      let(:stmt) do
        Stmt::Var.new(
          Token.new('myVar', 1, 'nil', TokenTypes::IDENTIFIER),
          Expr::Literal.new(123),
        )
      end

      it 'evaluates the initializer and defines it in @environment' do
        expect(subject).to eq(nil)
        expect(described_class.environment.class.values).to eq('myVar' => 123)
      end
    end

    context 'when stmt.initializer is not Expr' do
      let(:stmt) do
        Stmt::Var.new(
          Token.new('myVar', 1, 'nil', TokenTypes::IDENTIFIER),
          nil,
        )
      end

      it 'defines the statement lexeme in the @environment without evaluating it' do
        expect(subject).to eq(nil)
        expect(described_class.environment.class.values).to eq('myVar' => nil)
      end
    end
  end

  # @example
  # 
  # var a = 0.0;
  # var b = true;
  #
  # while (true) {
  #   print "hej";
  # }
  #
  describe '#visit_while_stmt' do
    subject { interpreter.visit_while_stmt(stmt) }

    let(:stmt) do
      Stmt::While.new(
        Expr::Binary.new(
          Expr::Variable.new(
            Token.new('b', 1, 'nil', TokenTypes::IDENTIFIER),
          ),
          Token.new('==', 4, 'nil', TokenTypes::EQUAL_EQUAL),
          Expr::Literal.new(true),
        ),
        Stmt::Block.new([
          Stmt::Expression.new(
            Expr::Assign.new(
              Token.new('a', 5, 'nil', TokenTypes::IDENTIFIER),
              Expr::Binary.new(
                Expr::Variable.new(
                  Token.new('a', 5, 'nil', TokenTypes::IDENTIFIER)
                ),
                Token.new('+', 5, 'nil', TokenTypes::PLUS),
                Expr::Literal.new(1.0),
              ),
            ),
          ),
          Stmt::Print.new(
            Expr::Variable.new(
              Token.new('a', 6, 'nil', TokenTypes::IDENTIFIER)
            ),
          ),
          Stmt::Expression.new(
            Expr::Assign.new(
              Token.new('b', 7, 'nil', TokenTypes::IDENTIFIER),
              Expr::Literal.new(false),
            ),
          ),
        ]),
      )
    end

    before do
      # @example
      #
      # var a = 0.0;
      # var b = true;
      #
      described_class.environment.class.values =
        { 'a' => 0.0, 'b' => true }
    end

    it 'evaluates the expression and outputs it to stdout' do
      expect { subject }
        .to output("1\n").to_stdout
    end
  end
end
