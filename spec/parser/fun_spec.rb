# frozen_string_literal: true

require_relative './../spec_helper'

require_relative './../../lib/lox'
require_relative './../../lib/parser'
require_relative './../../lib/token'

RSpec.describe Parser do
  describe 'FUN' do
    subject { described_class.new(tokens).parse }

    let(:tokens) do
      [
        Token.new('fun', 1, 'nil', TokenTypes::FUN),
        Token.new('fn',  1, 'nil', TokenTypes::IDENTIFIER),
        Token.new('(',   1, 'nil', TokenTypes::LEFT_PAREN),
        Token.new(')',   1, 'nil', TokenTypes::RIGHT_PAREN),
        Token.new('{',   1, 'nil', TokenTypes::LEFT_BRACE),
        Token.new('}',   1, 'nil', TokenTypes::RIGHT_BRACE),
        Token.new('',    1, 'nil', TokenTypes::EOF),
      ]
    end
    let(:expected_output) do
      Stmt::Function.new(
        Token.new('fn', 1, nil, TokenTypes::IDENTIFIER),
        [],
        [],
      )
    end

    it 'correctly parses the tokens' do
      expect(subject).to eq([expected_output])
    end

    context 'with +kind+ as a non-identifier' do
      let(:tokens) do
        [
          Token.new('fun', 1, 'nil', TokenTypes::FUN),
          Token.new('1',   1, 'nil', TokenTypes::NUMBER),
          Token.new('(',   1, 'nil', TokenTypes::LEFT_PAREN),
          Token.new(')',   1, 'nil', TokenTypes::RIGHT_PAREN),
          Token.new('{',   1, 'nil', TokenTypes::LEFT_BRACE),
          Token.new('}',   1, 'nil', TokenTypes::RIGHT_BRACE),
          Token.new('',    1, 'nil', TokenTypes::EOF),
        ]
      end

      it 'raises an error' do
        expect { subject }
          .to output("[line 1] Error at '1': Expect function name.\n")
          .to_stdout
      end
    end

    context "missing '(' after function name" do
      let(:tokens) do
        [
          Token.new('fun', 1, 'nil', TokenTypes::FUN),
          Token.new('1',   1, 'nil', TokenTypes::NUMBER),
          Token.new('(',   1, 'nil', TokenTypes::LEFT_PAREN),
          Token.new(')',   1, 'nil', TokenTypes::RIGHT_PAREN),
          Token.new('{',   1, 'nil', TokenTypes::LEFT_BRACE),
          Token.new('}',   1, 'nil', TokenTypes::RIGHT_BRACE),
          Token.new('',    1, 'nil', TokenTypes::EOF),
        ]
      end

      it 'raises an error' do
        expect { subject }
          .to output("[line 1] Error at '1': Expect function name.\n")
          .to_stdout
      end
    end

    context 'having more than 254 parameters' do
      let(:tokens) do
        [
          Token.new('fun', 1, 'nil', TokenTypes::FUN),
          Token.new('fn',  1, 'nil', TokenTypes::IDENTIFIER),
          Token.new('(',   1, 'nil', TokenTypes::LEFT_PAREN),
          args,
          Token.new(')',   1, 'nil', TokenTypes::RIGHT_PAREN),
          Token.new('{',   1, 'nil', TokenTypes::LEFT_BRACE),
          Token.new('}',   1, 'nil', TokenTypes::RIGHT_BRACE),
          Token.new('',    1, 'nil', TokenTypes::EOF),
        ].flatten
      end
      let(:args) do
        1.upto(256).each_with_object([]) do |index, arr|
          arr.push(
            Token.new("a#{index}", 1, 'nil', TokenTypes::IDENTIFIER),
          )

          unless index == 256
            arr.push(
              Token.new(',', 1, 'nil', TokenTypes::COMMA),
            )
          end
        end
      end

      it 'raises an error' do
        expect { subject }
          .to output("[line 1] Error at 'a256': Can't have more than 255 parameters.\n")
          .to_stdout
      end
    end

    context 'with parameters as non-identifiers' do
      let(:tokens) do
        [
          Token.new('fun', 1, 'nil', TokenTypes::FUN),
          Token.new('fn',  1, 'nil', TokenTypes::IDENTIFIER),
          Token.new('(',   1, 'nil', TokenTypes::LEFT_PAREN),
          Token.new('1',   1, 'nil', TokenTypes::NUMBER),
          Token.new(')',   1, 'nil', TokenTypes::RIGHT_PAREN),
          Token.new('{',   1, 'nil', TokenTypes::LEFT_BRACE),
          Token.new('}',   1, 'nil', TokenTypes::RIGHT_BRACE),
          Token.new('',    1, 'nil', TokenTypes::EOF),
        ].flatten
      end

      it 'raises an error' do
        expect { subject }
          .to output("[line 1] Error at '1': Expect parameter name.\n")
          .to_stdout
      end
    end

    context "missing ')' after parameters" do
      let(:tokens) do
        [
          Token.new('fun', 1, 'nil', TokenTypes::FUN),
          Token.new('fn',  1, 'nil', TokenTypes::IDENTIFIER),
          Token.new('(',   1, 'nil', TokenTypes::LEFT_PAREN),
          Token.new('arg', 1, 'nil', TokenTypes::IDENTIFIER),
          Token.new('{',   1, 'nil', TokenTypes::LEFT_BRACE),
          Token.new('}',   1, 'nil', TokenTypes::RIGHT_BRACE),
          Token.new('',    1, 'nil', TokenTypes::EOF),
        ].flatten
      end

      it 'raises an error' do
        expect { subject }
          .to output("[line 1] Error at '{': Expect ')' after parameters.\n")
          .to_stdout
      end
    end

    context "missing '{' before function body" do
      let(:tokens) do
        [
          Token.new('fun', 1, 'nil', TokenTypes::FUN),
          Token.new('fn',  1, 'nil', TokenTypes::IDENTIFIER),
          Token.new('(',   1, 'nil', TokenTypes::LEFT_PAREN),
          Token.new(')',   1, 'nil', TokenTypes::RIGHT_PAREN),
          Token.new('',    1, 'nil', TokenTypes::EOF),
        ].flatten
      end

      it 'raises an error' do
        expect { subject }
          .to output("[line 1] Error at end: Expect '{' before function body.\n")
          .to_stdout
      end
    end
  end
end
