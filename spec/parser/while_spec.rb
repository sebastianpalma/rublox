# frozen_string_literal: true

require_relative './../spec_helper'

require_relative './../../lib/parser'
require_relative './../../lib/token'
require_relative './../../lib/lox'

RSpec.describe Parser do
  describe 'WHILE' do
    subject { described_class.new(tokens).parse }

    let(:tokens) do
      [
        Token.new('while', 1, 'nil', TokenTypes::WHILE),
        Token.new('(',     1, 'nil', TokenTypes::LEFT_PAREN),
        Token.new('true',  1, 'nil', TokenTypes::TRUE),
        Token.new(')',     1, 'nil', TokenTypes::RIGHT_PAREN),
        Token.new('{',     1, 'nil', TokenTypes::LEFT_BRACE),
        Token.new('}',     1, 'nil', TokenTypes::RIGHT_BRACE),
        Token.new('',      1, 'nil', TokenTypes::EOF),
      ]
    end
    let(:expected_output) do
      Stmt::While.new(
        Expr::Literal.new(true),
        Stmt::Block.new([]),
      )
    end

    it 'correctly parses the tokens' do
      expect(subject).to eq([expected_output])
    end

    context "missing '(' after 'while'" do
      let(:tokens) do
        [
          Token.new('while', 1, 'nil', TokenTypes::WHILE),
          Token.new('',      1, 'nil', TokenTypes::EOF),
        ]
      end

      it 'raises an error' do
        expect { subject }
          .to output("[line 1] Error at end: Expect '(' after 'while'.\n")
          .to_stdout
      end
    end

    context "missing ')' after condition" do
      let(:tokens) do
        [
          Token.new('while', 1, 'nil', TokenTypes::WHILE),
          Token.new('(',     1, 'nil', TokenTypes::LEFT_PAREN),
          Token.new('true',  1, 'nil', TokenTypes::TRUE),
          Token.new('',      1, 'nil', TokenTypes::EOF),
        ]
      end

      it 'raises an error' do
        expect { subject }
          .to output("[line 1] Error at end: Expect ')' after condition.\n")
          .to_stdout
      end
    end
  end
end
