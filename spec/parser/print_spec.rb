# frozen_string_literal: true

require_relative './../spec_helper'

require_relative './../../lib/parser'
require_relative './../../lib/token'

RSpec.describe Parser do
  describe 'PRINT' do
    subject { described_class.new(tokens).parse }

    let(:tokens) do
      [
        Token.new('print', 1, 'nil', TokenTypes::PRINT),
        Token.new('true',  1, 'nil', TokenTypes::TRUE),
        Token.new(';',     1, 'nil', TokenTypes::SEMICOLON),
        Token.new('',      2, 'nil', TokenTypes::EOF),
      ]
    end
    let(:output) do
      Stmt::Print.new(
        Expr::Literal.new(true),
      )
    end

    it 'correctly parses the tokens' do
      expect(subject).to eq([output])
    end
  end
end
