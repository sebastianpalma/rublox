# frozen_string_literal: true

require_relative './../spec_helper'

require_relative './../../lib/lox'
require_relative './../../lib/parser'
require_relative './../../lib/token'

RSpec.describe Parser do
  describe 'PRINT' do
    subject { described_class.new(tokens).parse }

    let(:tokens) do
      [
        Token.new('fun',    1, nil, TokenTypes::FUN),
        Token.new('fn',     1, nil, TokenTypes::IDENTIFIER),
        Token.new('(',      1, nil, TokenTypes::LEFT_PAREN),
        Token.new(')',      1, nil, TokenTypes::RIGHT_PAREN),
        Token.new('{',      1, nil, TokenTypes::LEFT_BRACE),
        Token.new('return', 1, nil, TokenTypes::RETURN),
        Token.new('1',      1, 1.0, TokenTypes::NUMBER),
        Token.new(';',      1, nil, TokenTypes::SEMICOLON),
        Token.new('}',      1, nil, TokenTypes::RIGHT_BRACE),
        Token.new('',       1, nil, TokenTypes::EOF),
      ]
    end
    let(:expected_output) do
      Stmt::Function.new(
        Token.new('fn', 1, nil, TokenTypes::IDENTIFIER),
        [],
        [
          Stmt::Return.new(
            Token.new('return', 1, nil, TokenTypes::RETURN),
            Expr::Literal.new(1.0),
          )
        ],
      )
    end

    it 'correctly parses the tokens' do
      expect(subject).to eq([expected_output])
    end

    context "missing ';' after return value" do
      let(:tokens) do
        [
          Token.new('fun',    1, nil, TokenTypes::FUN),
          Token.new('fn',     1, nil, TokenTypes::IDENTIFIER),
          Token.new('(',      1, nil, TokenTypes::LEFT_PAREN),
          Token.new(')',      1, nil, TokenTypes::RIGHT_PAREN),
          Token.new('{',      1, nil, TokenTypes::LEFT_BRACE),
          Token.new('return', 1, nil, TokenTypes::RETURN),
          Token.new('1',      1, 1.0, TokenTypes::NUMBER),
          Token.new('}',      1, nil, TokenTypes::RIGHT_BRACE),
          Token.new('',       1, nil, TokenTypes::EOF),
        ]
      end

      it 'raises an error' do
        expect { subject }
          .to output(<<~TXT)
            [line 1] Error at '}': Expect ';' after return value.
            [line 1] Error at end: Expect '}' after block.
          TXT
          .to_stdout
      end
    end
  end
end
