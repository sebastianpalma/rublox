# frozen_string_literal: true

require_relative './../spec_helper'

require_relative './../../lib/parser'
require_relative './../../lib/token'
require_relative './../../lib/lox'

RSpec.describe Parser do
  describe 'FOR' do
    subject { described_class.new(tokens).parse }

    let(:tokens) do
      [
        Token.new('for', 1, 'nil', TokenTypes::FOR),
        Token.new('(',   1, 'nil', TokenTypes::LEFT_PAREN),
        Token.new('var', 1, 'nil', TokenTypes::VAR),
        Token.new('a',   1, 'nil', TokenTypes::IDENTIFIER),
        Token.new('=',   1, 'nil', TokenTypes::EQUAL),
        Token.new('1',   1, 1.0,   TokenTypes::NUMBER),
        Token.new(';',   1, 'nil', TokenTypes::SEMICOLON),
        Token.new('a',   1, 'nil', TokenTypes::IDENTIFIER),
        Token.new('<',   1, 'nil', TokenTypes::LESS),
        Token.new('2',   1, 2.0,   TokenTypes::NUMBER),
        Token.new(';',   1, 'nil', TokenTypes::SEMICOLON),
        Token.new('a',   1, 'nil', TokenTypes::IDENTIFIER),
        Token.new('+',   1, 'nil', TokenTypes::PLUS),
        Token.new('1',   1, 1.0,   TokenTypes::NUMBER),
        Token.new(')',   1, 'nil', TokenTypes::RIGHT_PAREN),
        Token.new('{',   1, 'nil', TokenTypes::LEFT_BRACE),
        Token.new('}',   2, 'nil', TokenTypes::RIGHT_BRACE),
        Token.new('',    1, 'nil', TokenTypes::EOF),
      ]
    end
    let(:expected_output) do
      Stmt::Block.new([
        Stmt::Var.new(
          Token.new('a', 1, 'nil', TokenTypes::IDENTIFIER),
          Expr::Literal.new(1.0),
        ),
        Stmt::While.new(
          Expr::Binary.new(
            Expr::Variable.new(
              Token.new('a', 1, 'nil', TokenTypes::IDENTIFIER),
            ),
            Token.new('<', 1, 'nil', TokenTypes::LESS),
            Expr::Literal.new(2.0),
          ),
          Stmt::Block.new([
            Stmt::Block.new([]),
            Stmt::Expression.new(
              Expr::Binary.new(
                Expr::Variable.new(
                  Token.new('a', 1, 'nil', TokenTypes::IDENTIFIER),
                ),
                Token.new('+', 1, 'nil', TokenTypes::PLUS),
                Expr::Literal.new(1.0),
              ),
            ),
          ]),
        ),
      ])
    end

    it 'correctly parses the tokens' do
      expect(subject).to eq([expected_output])
    end

    context "missing '(' after 'for'" do
      let(:tokens) do
        [
          Token.new('for', 1, 'nil', TokenTypes::FOR),
          Token.new('',    1, 'nil', TokenTypes::EOF),
        ]
      end

      it 'raises an error' do
        expect { subject }
          .to output("[line 1] Error at end: Expect '(' after 'for'.\n")
          .to_stdout
      end
    end

    context "missing ';' after 'for' loop condition" do
      let(:tokens) do
        [
          Token.new('for', 1, 'nil', TokenTypes::FOR),
          Token.new('(',   1, 'nil', TokenTypes::LEFT_PAREN),
          Token.new('var', 1, 'nil', TokenTypes::VAR),
          Token.new('a',   1, 'nil', TokenTypes::IDENTIFIER),
          Token.new('=',   1, 'nil', TokenTypes::EQUAL),
          Token.new('1',   1, 1.0,   TokenTypes::NUMBER),
          Token.new(';',   1, 'nil', TokenTypes::SEMICOLON),
          Token.new('a',   1, 'nil', TokenTypes::IDENTIFIER),
          Token.new('<',   1, 'nil', TokenTypes::LESS),
          Token.new('2',   1, 2.0,   TokenTypes::NUMBER),
          Token.new(')',   1, 'nil', TokenTypes::RIGHT_PAREN),
          Token.new('{',   1, 'nil', TokenTypes::LEFT_BRACE),
          Token.new('}',   1, 'nil', TokenTypes::RIGHT_BRACE),
          Token.new('',    1, 'nil', TokenTypes::EOF),
        ]
      end

      it 'raises an error' do
        expect { subject }
          .to output("[line 1] Error at ')': Expect ';' after loop condition.\n")
          .to_stdout
      end
    end

    context 'nil condition' do
      let(:tokens) do
        [
          Token.new('for', 1, 'nil', TokenTypes::FOR),
          Token.new('(',   1, 'nil', TokenTypes::LEFT_PAREN),
          Token.new('var', 1, 'nil', TokenTypes::VAR),
          Token.new('a',   1, 'nil', TokenTypes::IDENTIFIER),
          Token.new('=',   1, 'nil', TokenTypes::EQUAL),
          Token.new('1',   1, 1.0,   TokenTypes::NUMBER),
          Token.new(';',   1, 'nil', TokenTypes::SEMICOLON),
          Token.new(';',   1, 'nil', TokenTypes::SEMICOLON),
          Token.new('a',   1, 'nil', TokenTypes::IDENTIFIER),
          Token.new('=',   1, 'nil',   TokenTypes::EQUAL),
          Token.new('a',   1, 'nil', TokenTypes::IDENTIFIER),
          Token.new('+',   1, 'nil', TokenTypes::PLUS),
          Token.new('1',   1, 1.0,   TokenTypes::NUMBER),
          Token.new(')',   1, 'nil', TokenTypes::RIGHT_PAREN),
          Token.new('{',   1, 'nil', TokenTypes::LEFT_BRACE),
          Token.new('}',   1, 'nil', TokenTypes::RIGHT_BRACE),
          Token.new('',    1, 'nil', TokenTypes::EOF),
        ]
      end

      it 'adds a true literal as the condition' do
        expect(subject[0].statements[1].condition)
          .to eq(Expr::Literal.new(true))
      end
    end
  end
end
