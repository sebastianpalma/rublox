# frozen_string_literal: true

require_relative './../spec_helper'

require_relative './../../lib/lox'
require_relative './../../lib/parser'
require_relative './../../lib/token'
require_relative './../../lib/stmt/l_class'

RSpec.describe Parser do
  describe 'CLASS' do
    subject { described_class.new(tokens).parse }

    context 'without class methods' do
      # @example
      #
      # class DevonshireCream {}
      #
      let(:tokens) do
        [
          Token.new('class',            1, 'nil', TokenTypes::CLASS),
          Token.new('DevonshireCream',  1, 'nil', TokenTypes::IDENTIFIER),
          Token.new('{',                1, 'nil', TokenTypes::LEFT_BRACE),
          Token.new('}',                1, 'nil', TokenTypes::RIGHT_BRACE),
          Token.new('',                 2, 'nil', TokenTypes::EOF),
        ]
      end
      let(:expected_output) do
        Stmt::LClass.new(
          Token.new('DevonshireCream', 1, 'nil', TokenTypes::IDENTIFIER),
          nil,
          [],
        )
      end

      it 'correctly parses the tokens' do
        expect(subject).to eq([expected_output])
      end
    end

    context 'with class methods' do
      # @example
      #
      # class DevonshireCream {
      #   serveOn() {
      #     return "hej";
      #   }
      #
      #   greet() {
      #     return "hej";
      #   }
      # }
      #
      let(:tokens) do
        [
          Token.new('class',           1, 'nil', TokenTypes::CLASS),
          Token.new('DevonshireCream', 1, 'nil', TokenTypes::IDENTIFIER),
          Token.new('{',               1, 'nil', TokenTypes::LEFT_BRACE),
          Token.new('serveOn',         2, 'nil', TokenTypes::IDENTIFIER),
          Token.new('(',               2, 'nil', TokenTypes::LEFT_PAREN),
          Token.new(')',               2, 'nil', TokenTypes::RIGHT_PAREN),
          Token.new('{',               2, 'nil', TokenTypes::LEFT_BRACE),
          Token.new('return',          3, 'nil', TokenTypes::RETURN),
          Token.new('"hej"',           3, 'hej', TokenTypes::STRING),
          Token.new(';',               3, 'nil', TokenTypes::SEMICOLON),
          Token.new('}',               4, 'nil', TokenTypes::RIGHT_BRACE),
          Token.new('greet',           6, 'nil', TokenTypes::IDENTIFIER),
          Token.new('(',               6, 'nil', TokenTypes::LEFT_PAREN),
          Token.new(')',               6, 'nil', TokenTypes::RIGHT_PAREN),
          Token.new('{',               6, 'nil', TokenTypes::LEFT_BRACE),
          Token.new('return',          7, 'nil', TokenTypes::RETURN),
          Token.new('"hej"',           7, 'hej', TokenTypes::STRING),
          Token.new(';',               7, 'nil', TokenTypes::SEMICOLON),
          Token.new('}',               8, 'nil', TokenTypes::RIGHT_BRACE),
          Token.new('}',               9, 'nil', TokenTypes::RIGHT_BRACE),
          Token.new('',                9, 'nil', TokenTypes::EOF),
        ]
      end
      let(:expected_output) do
        Stmt::LClass.new(
          Token.new('DevonshireCream', 1, nil, TokenTypes::IDENTIFIER),
          nil,
          [
            Stmt::Function.new(
              Token.new('serveOn', 2, nil, TokenTypes::IDENTIFIER),
              [],
              [
                Stmt::Return.new(
                  Token.new('return', 3, nil, TokenTypes::RETURN),
                  Expr::Literal.new('hej'),
                ),
              ],
            ),
            Stmt::Function.new(
              Token.new('greet', 6, nil, TokenTypes::IDENTIFIER),
              [],
              [
                Stmt::Return.new(
                  Token.new('return', 7, nil, TokenTypes::RETURN),
                  Expr::Literal.new('hej'),
                ),
              ],
            ),
          ],
        )
      end

      it 'correctly parses the tokens' do
        expect(subject).to eq([expected_output])
      end
    end

    context 'without a class name' do
      let(:tokens) do
        [
          Token.new('class',            1, 'nil', TokenTypes::CLASS),
          Token.new('{',                1, 'nil', TokenTypes::LEFT_BRACE),
          Token.new('}',                1, 'nil', TokenTypes::RIGHT_BRACE),
          Token.new('',                 2, 'nil', TokenTypes::EOF),
        ]
      end

      it 'raises an exception' do
        expect { subject }
          .to output("[line 1] Error at '{': Expect class name.\n")
          .to_stdout
      end
    end

    context 'without "{" before class body' do
      let(:tokens) do
        [
          Token.new('class',            1, 'nil', TokenTypes::CLASS),
          Token.new('DevonshireCream',  1, 'nil', TokenTypes::IDENTIFIER),
          Token.new('}',                1, 'nil', TokenTypes::RIGHT_BRACE),
          Token.new('',                 2, 'nil', TokenTypes::EOF),
        ]
      end

      it 'raises an exception' do
        expect { subject }
          .to output("[line 1] Error at '}': Expect '{' before class body.\n")
          .to_stdout
      end
    end

    context 'without "}" after class body' do
      let(:tokens) do
        [
          Token.new('class',            1, 'nil', TokenTypes::CLASS),
          Token.new('DevonshireCream',  1, 'nil', TokenTypes::IDENTIFIER),
          Token.new('{',                1, 'nil', TokenTypes::LEFT_BRACE),
          Token.new('',                 2, 'nil', TokenTypes::EOF),
        ]
      end

      it 'raises an exception' do
        expect { subject }
          .to output("[line 2] Error at end: Expect '}' after class body.\n")
          .to_stdout
      end
    end
  end
end
