# frozen_string_literal: true

require_relative './../spec_helper'

require_relative './../../lib/parser'
require_relative './../../lib/token'

RSpec.describe Parser do
  describe 'LEFT_BRACE' do
    subject { described_class.new(tokens).parse }

    let(:tokens) do
      [
        Token.new('{', 1, 'nil', TokenTypes::LEFT_BRACE),
        Token.new('}', 1, 'nil', TokenTypes::RIGHT_BRACE),
        Token.new('',  1, 'nil', TokenTypes::EOF),
      ]
    end
    let(:output) do
      Stmt::Block.new([])
    end

    it 'correctly parses the tokens' do
      expect(subject).to eq([output])
    end
  end
end
