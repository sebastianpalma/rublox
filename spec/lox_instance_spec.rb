# frozen_string_literal: true

require_relative './spec_helper'

require_relative './../lib/lox_instance'
require_relative './../lib/token'
require_relative './../lib/token_types'
require_relative './../lib/stmt/function'
require_relative './../lib/stmt/print'
require_relative './../lib/expr/literal'

RSpec.describe LoxInstance do
  skip 'TBD'
end
