# frozen_string_literal: true

require_relative './../spec_helper'

require_relative './../../lib/lox'

RSpec.describe Lox do
  subject { described_class.new([file]).main }

  let(:file) { 'spec/support/files/integration/file.lox' }

  before { File.open(file, 'w') { |f| f.puts(file_content) } }

  after { File.delete(file) }

  context 'explicit return' do
    let(:file_content) do
      <<~CODE
        fun f(g) {
          var n = 0;
          while (true) {
            if (n == 1) {
              return "hej";
            }
            n = n + 1;
          }
        }
        print f(1);
      CODE
    end

    it 'explicitly returns from the iteration with the given value' do
      expect { subject }.to output("hej\n").to_stdout
    end
  end

  context 'initializing a variable more than once in same scope' do
    let(:file_content) do
      <<~CODE
        fun hej(g) {
          var n = "0";
          var n = "1";
        }
        print hej(1);
      CODE
    end

    it 'returns early printing the error' do
      expect { subject }
        .to output(<<~TEXT)
          [line 3] Error at 'n': Already a variable with this name in this scope.
        TEXT
        .to_stdout
    end
  end

  context 'top-level return' do
    let(:file_content) do
      <<~CODE
        return "at top level";
      CODE
    end

    it 'returns early printing the error' do
      expect { subject }
        .to output(<<~TEXT)
          [line 1] Error at 'return': Can't return from top-level code.
        TEXT
        .to_stdout
    end
  end

  context 'multiple functions with same name in different scopes' do
    let(:file_content) do
      <<~CODE
        fun hej(a, b) {
          return a + b;
        }
        {
          fun hej() {
            return "inner";
          }
          print hej();
        }
      CODE
    end

    it 'returns early printing the error' do
      expect { subject }
        .to output("inner\n")
        .to_stdout
    end
  end

  context do
    let(:file_content) do
      <<~CODE
        class Hej {
          hej() {
            return "hej";
          }
        }

        print Hej;
      CODE
    end

    it 'prints the stringified class representation' do
      expect { subject }
        .to output("Hej\n")
        .to_stdout
    end
  end

  context do
    let(:file_content) do
      <<~CODE
        class Hej {}
        var hej = Hej();
        print hej;
      CODE
    end

    it 'supports class instances' do
      expect { subject }
        .to output("Hej instance\n")
        .to_stdout
    end
  end

  context 'instance methods' do
    let(:file_content) do
      <<~CODE
        class Bacon {
          eat() {
            print "hej!";
          }
        }

        Bacon().eat();
      CODE
    end

    it 'supports invoking instance methods' do
      expect { subject }
        .to output("hej!\n")
        .to_stdout
    end
  end

  context 'this keyword' do
    let(:file_content) do
      <<~CODE
        class Cake {
          taste() {
            var adjective = "delicious";
            print "The " + this.flavor + " cake is " + adjective + "!";
          }
        }

        var cake = Cake();
        cake.flavor = "German chocolate";
        cake.taste();
      CODE
    end

    it do
      expect { subject }
        .to output("The German chocolate cake is delicious!\n")
        .to_stdout
    end
  end

  context 'methods and functions' do
    let(:file_content) do
      <<~CODE
        class Thing {
          getCallback() {
            fun localFunction() {
              print this;
            }

            return localFunction;
          }
        }

        var callback = Thing().getCallback();
        callback();
      CODE
    end

    it do
      expect { subject }
        .to output("Thing instance\n")
        .to_stdout
    end
  end

  context 'unexpected +this+' do
    context 'at top-level' do
      let(:file_content) do
        <<~CODE
          print this;
        CODE
      end

      it do
        expect { subject }
          .to output("[line 1] Error at 'this': Can't use 'this' outside of a class.\n")
          .to_stdout
      end
    end

    context 'within a function at top-level' do
      let(:file_content) do
        <<~CODE
          fun notAMethod() {
            print this;
          }
        CODE
      end

      it do
        expect { subject }
          .to output("[line 2] Error at 'this': Can't use 'this' outside of a class.\n")
          .to_stdout
      end
    end
  end

  context 'invoking +init+ directly' do
    let(:file_content) do
      <<~CODE
        class Foo {
          init() {
            print this;
          }
        }

        var foo = Foo();
        print foo.init();
      CODE
    end

    it do
      expect { subject }
        .to output("Foo instance\nFoo instance\nFoo instance\n")
        .to_stdout
    end
  end

  context 'returning from +init+' do
    context 'returning a value' do
      let(:file_content) do
        <<~CODE
        class Foo {
          init() {
            return "something else";
          }
        } 
        print Foo().init();
        CODE
      end

      it 'raises an exception' do
        expect { subject }
          .to output("[line 3] Error at 'return': Can't return a value from an initializer.\n")
          .to_stdout
      end
    end

    context 'returning no value' do
      let(:file_content) do
        <<~CODE
          class Hej {
            init() {
              return;
            }
          }
          print Hej().init();
        CODE
      end

      it 'returns +this+' do
        expect { subject }
          .to output("Hej instance\n")
          .to_stdout
      end
    end

    context 'identifies inheritance' do
      let(:file_content) do
        <<~CODE
          class Hej {}
          class BostonCream < Hej {
          }
        CODE
      end

      it 'is parsed with no errors' do
        expect { subject }
          .not_to output
          .to_stdout
      end
    end

    context 'disallows inheriting from the same class' do
      let(:file_content) do
        <<~CODE
          class Hej < Hej {
          }
        CODE
      end

      it 'raises an error' do
        expect { subject }
          .to output("[line 1] Error at 'Hej': A class can't inherit from itself.\n")
          .to_stdout
      end
    end

    context "disallows inhering from smth that's not a class" do
      let(:file_content) do
        <<~CODE
          var NotAClass = "I am totally not a class";

          class Hej < NotAClass {}
        CODE
      end

      it 'raises an error' do
        expect { subject }
          .to output("Superclass must be a class.\n[line 3]\n")
          .to_stdout
      end
    end

    context 'recognizes fields methods from superclassess' do
      let(:file_content) do
        <<~CODE
          class Doughnut {
            hej() {
              print "Fry until golden brown.";
            }
          }

          class BostonCream < Doughnut {}

          BostonCream().hej();

        CODE
      end

      it 'correctly invokes the inherited method' do
        expect { subject }
          .to output("Fry until golden brown.\n")
          .to_stdout
      end
    end

    context 'supports the usage of +super+' do
      let(:file_content) do
        <<~CODE
          class Doughnut {
            cook() {
              print "Fry until golden brown.";
            }
          }

          class BostonCream < Doughnut {
            cook() {
              super.cook();
              print "Pipe full of custard and coat with chocolate.";
            }
          }

          BostonCream().cook();
        CODE
      end

      it 'correctly handles super usage within methods' do
        expect { subject }
          .to output("Fry until golden brown.\nPipe full of custard and coat with chocolate.\n")
          .to_stdout
      end
    end

    context 'disallows using +super+ in classes with no superclass' do
      let(:file_content) do
        <<~CODE
          class Hej {
            cook() {
              super.cook();
              print "Pipe full of crème pâtissière.";
            }
          }
        CODE
      end

      it 'raises an error' do
        expect { subject }
          .to output("[line 3] Error at 'super': Can't use 'super' in a class with no superclass.\n")
          .to_stdout
      end
    end
  end
end
