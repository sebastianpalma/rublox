# frozen_string_literal: true

require_relative './spec_helper'

require_relative './../lib/token_types'

RSpec.describe TokenTypes do
  it { expect(described_class::BANG).to eq(:bang) }
  it { expect(described_class::BANG_EQUAL).to eq(:bang_equal) }
  it { expect(described_class::COMMA).to eq(:comma) }
  it { expect(described_class::DOT).to eq(:dot) }
  it { expect(described_class::EQUAL).to eq(:equal) }
  it { expect(described_class::EQUAL_EQUAL).to eq(:equal_equal) }
  it { expect(described_class::EOF).to eq(:eof) }
  it { expect(described_class::GREATER).to eq(:greater) }
  it { expect(described_class::GREATER_EQUAL).to eq(:greater_equal) }
  it { expect(described_class::IDENTIFIER).to eq(:identifier) }
  it { expect(described_class::LEFT_BRACE).to eq(:left_brace) }
  it { expect(described_class::LEFT_PAREN).to eq(:left_paren) }
  it { expect(described_class::LESS).to eq(:less) }
  it { expect(described_class::LESS_EQUAL).to eq(:less_equal) }
  it { expect(described_class::MINUS).to eq(:minus) }
  it { expect(described_class::NUMBER).to eq(:number) }
  it { expect(described_class::PLUS).to eq(:plus) }
  it { expect(described_class::RIGHT_BRACE).to eq(:right_brace) }
  it { expect(described_class::RIGHT_PAREN).to eq(:right_paren) }
  it { expect(described_class::SEMICOLON).to eq(:semicolon) }
  it { expect(described_class::SLASH).to eq(:slash) }
  it { expect(described_class::STAR).to eq(:star) }
  it { expect(described_class::STRING).to eq(:string) }
  it { expect(described_class::AND).to eq(:and) }
  it { expect(described_class::CLASS).to eq(:class) }
  it { expect(described_class::ELSE).to eq(:else) }
  it { expect(described_class::FALSE).to eq(:false) }
  it { expect(described_class::FUN).to eq(:fun) }
  it { expect(described_class::FOR).to eq(:for) }
  it { expect(described_class::IF).to eq(:if) }
  it { expect(described_class::NIL).to eq(:nil) }
  it { expect(described_class::OR).to eq(:or) }
  it { expect(described_class::PRINT).to eq(:print) }
  it { expect(described_class::RETURN).to eq(:return) }
  it { expect(described_class::SUPER).to eq(:super) }
  it { expect(described_class::THIS).to eq(:this) }
  it { expect(described_class::TRUE).to eq(:true) }
  it { expect(described_class::VAR).to eq(:var) }
  it { expect(described_class::WHILE).to eq(:while) }
end
