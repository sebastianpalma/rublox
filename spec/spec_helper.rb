ENV['ENV'] = 'test'

require 'pry'

require_relative './../lib/environment'
require_relative './../lib/interpreter'
require_relative './../lib/resolver'

RSpec.shared_context 'suppressing output' do
  let!(:original_stderr) { $stderr }
  let!(:original_stdout) { $stdout }

  before { $stderr = $stdout = File.open(File::NULL, 'w') }

  after { $stderr, $stdout = original_stderr, original_stdout }
end

RSpec.configure do |config|
  config.after do
    Environment.values = {}
    Resolver.scopes = []
    Interpreter.locals = {}
  end
end
