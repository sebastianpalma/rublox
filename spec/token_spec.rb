# frozen_string_literal: true

require_relative './spec_helper'

require_relative './../lib/token'

RSpec.describe Token do
  describe '#literal' do
    subject do
      described_class
        .new(lexeme, line, literal, type)
        .send(:literal)
    end

    context 'when nil' do
      let(:lexeme) { '=' }
      let(:line) { 1 }
      let(:literal) { 'nil' }
      let(:type) { :equal }

      it { expect(subject).to eq('nil') }
    end

    context 'when not nil' do
      let(:lexeme) { '0' }
      let(:line) { 1 }
      let(:literal) { 0.0 }
      let(:type) { :number }

      it { expect(subject).to eq(0.0) }
    end
  end

  describe '#to_s' do
    subject { described_class.new(lexeme, line, literal, type).to_s }

    let(:lexeme) { ';' }
    let(:line) { 1 }
    let(:literal) { 'nil' }
    let(:type) { :semicolon }

    it { expect(subject).to eq('semicolon ; nil') }
  end
end
