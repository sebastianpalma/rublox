# frozen_string_literal: true

require_relative './../spec_helper'

require_relative './../../lib/expr/literal'
require_relative './../../lib/interpreter'
require_relative './../../lib/stmt/function'
require_relative './../../lib/stmt/l_class'
require_relative './../../lib/stmt/return'
require_relative './../../lib/token'
require_relative './../../lib/token_types'

RSpec.describe Stmt::LClass do
  describe '#accept' do
    subject do
      described_class
        .new(name, nil, [])
        .accept(visitor)
    end

    let(:name) { Token.new('DevonshireCream', nil, 1, TokenTypes::CLASS) }
    let(:visitor) { Interpreter.new }

    it 'implements a new LoxClass instance' do
      allow(LoxClass).to receive(:new)
      subject
      expect(LoxClass).to have_received(:new).with(name.lexeme, nil, {})
    end

    it 'returns nil' do
      expect(subject).to eq(nil)
    end
  end

  describe '#==' do
    context 'when names are different' do
      subject { l_class == other }

      let(:l_class) do
        described_class.new(
          Token.new('Dog', nil, 1, TokenTypes::CLASS),
          nil,
          [],
        )
      end
      let(:other) do
        described_class.new(
          Token.new('Cat', nil, 1, TokenTypes::CLASS),
          nil,
          [],
        )
      end

      it 'returns false' do
        expect(subject).to eq(false)
      end
    end

    context 'when methods are different' do
      subject { l_class == other }

      let(:l_class) do
        described_class.new(
          Token.new('Dog', nil, 1, TokenTypes::CLASS),
          nil,
          [
            Stmt::Function.new(
              Token.new('serveOn', nil, 1, TokenTypes::IDENTIFIER),
              [],
              [
                Stmt::Return.new(
                  Token.new('return', nil, 2, TokenTypes::RETURN),
                  Expr::Literal.new('Scones'),
                ),
              ],
            ),
          ],
        )
      end
      let(:other) do
        described_class.new(
          Token.new('Dog', nil, 1, TokenTypes::CLASS),
          nil,
          [],
        )
      end

      it 'returns false' do
        expect(subject).to eq(false)
      end
    end

    context 'when both names and methods are equal' do
      subject { l_class == other }

      let(:l_class) do
        described_class.new(
          Token.new('Dog', nil, 1, TokenTypes::CLASS),
          nil,
          [],
        )
      end
      let(:other) { l_class }

      it 'returns true' do
        expect(subject).to eq(true)
      end
    end
  end
end
