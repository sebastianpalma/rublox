# frozen_string_literal: true

require_relative './../spec_helper'

require_relative './../../lib/expr/literal'
require_relative './../../lib/interpreter'
require_relative './../../lib/stmt/print'
require_relative './../../lib/token'
require_relative './../../lib/token_types'

RSpec.describe Stmt::Print do
  describe '#accept' do
    subject { described_class.new(expression).accept(visitor) }

    let(:expression) { Expr::Literal.new(12.3) }
    let(:visitor) { Interpreter.new }

    it 'evaluates the statement expression and prints it to stdout' do
      expect { subject }.to output("12.3\n").to_stdout
    end

    include_context 'suppressing output' do
      it { expect(subject).to eq(nil) }
    end
  end
end
