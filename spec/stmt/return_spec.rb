# frozen_string_literal: true

require_relative './../spec_helper'

require_relative './../../lib/expr/literal'
require_relative './../../lib/interpreter'
require_relative './../../lib/stmt/return'
require_relative './../../lib/token'
require_relative './../../lib/token_types'

RSpec.describe Stmt::Return do
  describe '#accept' do
    subject do
      described_class
        .new(keyword, value)
        .accept(visitor)
    end

    let(:keyword) { Token.new('return', nil, 1, TokenTypes::IDENTIFIER) }
    let(:value) { Expr::Literal.new('hej') }
    let(:visitor) { Interpreter.new }

    it 'returns the given value' do
      expect { subject }.to raise_error(Return, 'hej')
    end
  end
end
