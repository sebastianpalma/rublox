# frozen_string_literal: true

require_relative './../spec_helper'

require_relative './../../lib/expr/literal'
require_relative './../../lib/interpreter'
require_relative './../../lib/lox_function'
require_relative './../../lib/stmt/function'
require_relative './../../lib/stmt/print'
require_relative './../../lib/token'
require_relative './../../lib/token_types'

RSpec.describe Stmt::Function do
  describe '#accept' do
    subject do
      described_class
        .new(name, params, body)
        .accept(visitor)
    end

    let(:visitor) { Interpreter.new }

    let(:body) do
      [
        Stmt::Print.new(
          Expr::Literal.new(1.0)
        )
      ]
    end
    let(:name) { Token.new('fn', nil, 1, TokenTypes::IDENTIFIER) }
    let(:params) { [] }

    it 'defines the given function in the interpreter environment' do
      # Just an empty Environment at the beginning.
      expect(Interpreter.environment).to eq(Environment.new)

      subject

      values = Interpreter.environment.class.values
      fn = values['fn']

      expect(fn).to be_a(LoxFunction)

      fn.instance_variable_get(:@closure).tap do |closure|
        expect(closure.enclosing).to eq(nil)
        expect(closure.class.values.key?('fn')).to eq(true)
      end

      fn.instance_variable_get(:@declaration).tap do |declaration|
        expect(declaration).to be_a(Stmt::Function)
        expect(declaration.instance_variable_get(:@body))
          .to eq([Stmt::Print.new( Expr::Literal.new(1.0))])
        expect(declaration.instance_variable_get(:@name))
          .to eq(Token.new('fn', nil, 1, TokenTypes::IDENTIFIER))
        expect(declaration.instance_variable_get(:@param)).to eq(nil)
      end
    end
  end
end
