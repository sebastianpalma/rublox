# frozen_string_literal: true

require_relative './../spec_helper'

require_relative './../../lib/expr/literal'
require_relative './../../lib/expr/variable'
require_relative './../../lib/interpreter'
require_relative './../../lib/stmt/block'
require_relative './../../lib/stmt/print'
require_relative './../../lib/stmt/var'
require_relative './../../lib/token'
require_relative './../../lib/token_types'

RSpec.describe Stmt::Block do
  describe '#accept' do
    subject { described_class.new(statements).accept(visitor) }

    # @example
    #
    # var myVar = 2.8;
    # print myVar;
    #
    let(:statements) do
      [
        Stmt::Var.new(
          # line #1 opens the block
          Token.new('myVar', 2, 'nil', TokenTypes::IDENTIFIER),
          Expr::Literal.new(2.8)
        ),
        Stmt::Print.new(
          Expr::Variable.new(
            Token.new('myVar', 3, 'nil', TokenTypes::IDENTIFIER)
          )
        )
      ]
    end
    let(:initializer) { Expr::Literal.new(83) }
    let(:visitor) { Interpreter.new }

    it 'evaluates the statement expression and prints it to stdout' do
      expect { subject }.to output("2.8\n").to_stdout
    end

    include_context 'suppressing output' do
      it { expect(subject).to eq(nil) }
    end
  end
end
