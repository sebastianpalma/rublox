# frozen_string_literal: true

require_relative './../spec_helper'

require_relative './../../lib/expr/assign'
require_relative './../../lib/expr/binary'
require_relative './../../lib/expr/literal'
require_relative './../../lib/expr/variable'
require_relative './../../lib/interpreter'
require_relative './../../lib/stmt/block'
require_relative './../../lib/stmt/expression'
require_relative './../../lib/stmt/print'
require_relative './../../lib/stmt/while'
require_relative './../../lib/token'
require_relative './../../lib/token_types'

RSpec.describe Stmt::While do
  describe '#accept' do
    subject do
      described_class.new(condition, body).accept(visitor)
    end

    # @example
    #
    # while (b == true) { ... }
    #
    let(:condition) do
      Expr::Binary.new(
        Expr::Variable.new(
          Token.new('b', 1, 'nil', TokenTypes::IDENTIFIER),
        ),
        Token.new('==', 4, 'nil', TokenTypes::EQUAL_EQUAL),
        Expr::Literal.new(true),
      )
    end
    # @example
    #
    # {
    #   a = a + 1;
    #   print a;
    #   b = false;
    # }
    let(:body) do
      Stmt::Block.new([
        Stmt::Expression.new(
          Expr::Assign.new(
            Token.new('a', 5, 'nil', TokenTypes::IDENTIFIER),
            Expr::Binary.new(
              Expr::Variable.new(
                Token.new('a', 5, 'nil', TokenTypes::IDENTIFIER)
              ),
              Token.new('+', 5, 'nil', TokenTypes::PLUS),
              Expr::Literal.new(1.0),
            ),
          ),
        ),
        Stmt::Print.new(
          Expr::Variable.new(
            Token.new('a', 6, 'nil', TokenTypes::IDENTIFIER)
          ),
        ),
        Stmt::Expression.new(
          Expr::Assign.new(
            Token.new('b', 7, 'nil', TokenTypes::IDENTIFIER),
            Expr::Literal.new(false),
          ),
        ),
      ])
    end
    let(:visitor) { Interpreter.new }

    context 'when true' do
      before do
        # @example
        #
        # var a = 0.0;
        # var b = true;
        #
        visitor.class.environment.class.values =
          { 'a' => 0.0, 'b' => true }
      end

      context 'with body' do
        it 'executes the block until the condition holds false' do
          expect { subject }
            .to output("1\n")
            .to_stdout
        end
      end
    end

    context 'when false' do
      before do
        # @example
        #
        # var a = 0.0;
        # var b = false;
        #
        visitor.class.environment.class.values =
          { 'a' => 0.0, 'b' => false }
      end

      context 'with body' do
        it 'does not execute the block' do
          expect { subject }.not_to output.to_stdout
        end
      end
    end
  end
end
