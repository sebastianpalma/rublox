# frozen_string_literal: true

require_relative './../spec_helper'

require_relative './../../lib/expr/literal'
require_relative './../../lib/interpreter'
require_relative './../../lib/stmt/block'
require_relative './../../lib/stmt/expression'
require_relative './../../lib/stmt/if'
require_relative './../../lib/stmt/print'
require_relative './../../lib/token'
require_relative './../../lib/token_types'

RSpec.describe Stmt::If do
  describe '#accept' do
    subject do
      described_class
        .new(condition, then_branch, else_branch)
        .accept(visitor)
    end

    let(:visitor) { Interpreter.new }

    context 'when true' do
      let(:condition) { Expr::Literal.new(true) }
      let(:else_branch) { nil }

      context 'with then branch' do
        let(:then_branch) do
          Stmt::Block.new([
            Stmt::Print.new(
              Expr::Literal.new('churre')
            )
          ])
        end

        it 'evaluates and return the then branch' do
          expect { subject }
            .to output("churre\n")
            .to_stdout
        end
      end

      context 'without then branch' do
        let(:then_branch) { Stmt::Block.new([]) }

        it 'returns nil' do
          expect(subject).to eq(nil)
        end
      end
    end

    context 'when false' do
      let(:condition) { Expr::Literal.new(false) }
      let(:then_branch) { Stmt::Block.new([]) }

      context 'with else branch' do
        let(:else_branch) do
          Stmt::Block.new([
            Stmt::Print.new(
              Expr::Literal.new('churre')
            )
          ])
        end

        it 'evaluates and return the then branch' do
          expect { subject }
            .to output("churre\n")
            .to_stdout
        end
      end

      context 'without else branch' do
        let(:else_branch) { Stmt::Block.new([]) }

        it 'returns nil' do
          expect(subject).to eq(nil)
        end
      end
    end
  end
end
