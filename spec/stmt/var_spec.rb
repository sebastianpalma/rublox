# frozen_string_literal: true

require_relative './../spec_helper'

require_relative './../../lib/expr/literal'
require_relative './../../lib/interpreter'
require_relative './../../lib/stmt/var'
require_relative './../../lib/token'
require_relative './../../lib/token_types'

RSpec.describe Stmt::Var do
  describe '#accept' do
    subject do
      described_class
        .new(name, initializer)
        .accept(visitor)
    end

    let(:name) { Token.new('myVar', nil, 1, TokenTypes::IDENTIFIER) }
    let(:initializer) { Expr::Literal.new('hej') }
    let(:visitor) { Interpreter.new }

    it { expect(subject).to eq(nil) }
  end
end
