# frozen_string_literal: true

require_relative './../spec_helper'

require_relative './../../lib/expr/literal'
require_relative './../../lib/interpreter'
require_relative './../../lib/expr/assign'
require_relative './../../lib/stmt/expression'
require_relative './../../lib/token'
require_relative './../../lib/token_types'

RSpec.describe Stmt::Expression do
  describe '#accept' do
    subject { described_class.new(expression).accept(visitor) }

    # var myVar = 1;
    # myVar = 2;
    let(:expression) do
      Expr::Assign.new(
        Token.new('myVar', 3, 'nil', TokenTypes::IDENTIFIER),
        Expr::Literal.new(2.0)
      )
    end
    let(:visitor) do
      Interpreter.new.tap do |interpreter|
        interpreter.class.environment.class.values = { 'myVar' => 7.0 }
      end
    end

    it 'evaluates the statement expressiont' do
      subject
      expect(visitor.class.environment.class.values['myVar']).to eq(2.0)
    end
  end
end
