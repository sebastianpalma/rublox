# frozen_string_literal: true

class Keywords
  AND = :and
  CLASS = :class
  ELSE = :else
  FALSE = :false
  FOR = :for
  FUN = :fun
  IF = :if
  NIL = :nil
  OR = :or
  PRINT = :print
  RETURN = :return
  SUPER = :super
  THIS = :this
  TRUE = :true
  VAR = :var
  WHILE = :while
end
