# frozen_string_literal: true

class ParserError < StandardError; end
