# frozen_string_literal: true

class RuntimeError < StandardError
  # @attr_reader [Token] token
  attr_reader :token

  def initialize(message, token)
    super(message)
    @token = token
  end
end
