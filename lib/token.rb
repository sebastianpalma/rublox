# frozen_string_literal: true

class Token
  attr_reader :lexeme, :line, :literal, :type

  def initialize(lexeme, line, literal, type)
    @lexeme = lexeme
    @line = line
    @literal = literal.nil? ? 'nil' : literal
    @type = type
  end

  def to_s
    "#{type} #{lexeme} #{literal}"
  end

  def ==(other)
    lexeme == other.lexeme &&
      line == other.line &&
      literal == other.literal &&
      type == other.type
  end
end
