# frozen_string_literal: true

require_relative './errors/runtime_error'
require_relative './environment'
require_relative './lox_class'
require_relative './lox_function'
require_relative './return'

class Interpreter
  @@globals = Environment.new
  @@environment = @@globals
  @@locals = {}

  class << self
    def globals
      @@globals
    end

    def globals=(other)
      @@globals = other
    end

    def environment
      @@environment
    end

    def environment=(other)
      @@environment = other
    end

    def locals
      @@locals
    end

    def locals=(other)
      @@locals = other
    end
  end

  def environment
    self.class.environment
  end

  def environment=(other)
    self.class.environment = other
  end

  # @param [Integer] depth
  # @param [Expr] expr
  def resolve(expr, depth)
    @@locals[expr] = depth
  end

  # @param [Array<Stmt>] statements
  def interpret(statements)
    statements.each do |statement|
      execute(statement)
    end
  rescue RuntimeError => error
    Lox.runtime_error(error)
  end

  # @param [Array<Stmt>] statements
  # @param [Environment] environment
  def execute_block(statements, environment)
    previous = @@environment

    begin
      @@environment = environment

      statements.each do |statement|
        execute(statement)
      end
    ensure
      @@environment = previous
    end
  end

  #
  # Expressions

  # @param [Expr::Assign] expr
  def visit_assign_expr(expr)
    value = evaluate(expr.value)

    distance = @@locals[expr]

    if distance.nil?
      @@globals.assign(expr.name, value)
    else
      @@environment.assign_at(distance, expr.name, value)
    end

    value
  end

  # @param [Expr::Binary] expr
  def visit_binary_expr(expr)
    left = evaluate(expr.left)
    right = evaluate(expr.right)

    case expr.operator.type
    when TokenTypes::GREATER
      check_number_operands(expr.operator, left, right)
      left.to_f > right.to_f
    when TokenTypes::GREATER_EQUAL
      check_number_operands(expr.operator, left, right)
      left.to_f >= right.to_f
    when TokenTypes::LESS
      check_number_operands(expr.operator, left, right)
      left.to_f < right.to_f
    when TokenTypes::LESS_EQUAL
      check_number_operands(expr.operator, left, right)
      left.to_f <= right.to_f
    when TokenTypes::MINUS
      check_number_operands(expr.operator, left, right)
      left.to_f - right.to_f
    when TokenTypes::PLUS
      if left.is_a?(Float) && right.is_a?(Float)
        return left + right
      end

      if left.is_a?(String) && right.is_a?(String)
        return "#{left}#{right}"
      end

      raise RuntimeError.new(
        'Operands must be two numbers or two strings',
        expr.operator.lexeme
      )
    when TokenTypes::SLASH
      check_number_operands(expr.operator, left, right)
      left.to_f / right.to_f
    when TokenTypes::STAR
      check_number_operands(expr.operator, left, right)
      left.to_f * right.to_f
    when TokenTypes::BANG_EQUAL
      left != right
    when TokenTypes::EQUAL_EQUAL
      left == right
    end
  end

  # @param [Expr::Call] expr
  def visit_call_expr(expr)
    function = evaluate(expr.callee)
    arguments = expr.arguments.map do |argument|
      evaluate(argument)
    end

    unless function.is_a?(LoxFunction) || function.is_a?(LoxClass)
      raise StandardError, 'Can only call functions and classes.'
    end

    unless arguments.size == function.arity
      raise(
        StandardError,
        "Expected #{function.arity} arguments but got #{arguments.size}."
      )
    end

    function.call(self, arguments)
  end

  # @param [Expr::Get] expr
  def visit_get_expr(expr)
    object = evaluate(expr.object)

    return object.get(expr.name) if object.is_a?(LoxInstance)

    raise StandardError, 'Only instances have properties'
  end

  # @param [Expr::Grouping] expr
  def visit_grouping_expr(expr) = evaluate(expr.expression)

  # @param [Expr::Literal] expr
  def visit_literal_expr(expr) = expr.value

  # @param [Expr::Logical] expr
  def visit_logical_expr(expr)
    left = evaluate(expr.left)

    if expr.operator.type == TokenTypes::OR
      return left if left
    else
      return left unless left
    end

    evaluate(expr.right)
  end

  # [TODO]: test.
  # @param [Expr::Set] expr
  def visit_set_expr(expr)
    object = evaluate(expr.object)

    unless object.is_a?(LoxInstance)
      raise StandardError, 'Only instances have fields'
    end

    value = evaluate(expr.value)
    object.set(expr.name, value)
    value
  end

  # [TODO]: test.
  # @param [Expr::Super] expr
  def visit_super_expr(expr)
    distance = @@locals[expr]

    superclass = @@environment.get_at(distance, 'super')
    object = @@environment.get_at(distance - 1, 'this')
    l_method = superclass.find_method(expr.l_method.lexeme)

    if l_method.nil?
      raise RuntimeError.new(
        "Undefined property #{expr.l_method.lexeme}.",
        expr.l_method,
      )
    end

    l_method.bind(object)
  end

  # [TODO]: test.
  # @param [Expr::This] expr
  def visit_this_expr(expr)
    look_up_variable(expr.keyword, expr)
  end

  # @param [Expr::Unary] expr
  def visit_unary_expr(expr)
    right = evaluate(expr.right)

    case expr.operator.type
    when TokenTypes::BANG
      !right
    when TokenTypes::MINUS
      check_number_operand(expr.operator, right)
      -right.to_f
    end
  end

  # @param [Expr::Variable] expr
  def visit_variable_expr(expr)
    look_up_variable(expr.name, expr)
  end

  # @param [Expr] expr
  # @param [Token] name
  def look_up_variable(name, expr)
    distance = @@locals[expr]
    
    if distance.nil?
      @@globals.get(name)
    else
      @@environment.get_at(distance, name.lexeme)
    end
  end

  #
  # Statements

  # @param [Stmt::Block] stmt
  def visit_block_stmt(stmt)
    execute_block(stmt.statements, Environment.new(@@environment))
    return
  end

  # @param [Stmt::Expression] stmt
  def visit_expression_stmt(stmt)
    evaluate(stmt.expression)
    return
  end

  # @param [Stmt::Function] stmt
  def visit_function_stmt(stmt)
    function = LoxFunction.new(stmt, @@environment, false)
    @@environment.define(stmt.name.lexeme, function)
    return
  end

  # @param [Stmt::If] stmt
  def visit_if_stmt(stmt)
    if evaluate(stmt.condition)
      execute(stmt.then_branch)
      return
    end

    unless stmt.else_branch.nil?
      execute(stmt.else_branch)
      return
    end
  end

  # @param [Stmt::LClass] stmt
  def visit_l_class_stmt(stmt)
    superclass = nil
    unless stmt.superclass.nil?
      superclass = evaluate(stmt.superclass)

      unless superclass.is_a?(LoxClass)
        raise RuntimeError.new('Superclass must be a class.', stmt.superclass.name)
      end
    end

    @@environment.define(stmt.name.lexeme, nil)

    unless stmt.superclass.nil?
      @@environment = Environment.new(@@environment)
      @@environment.define('super', superclass)
    end

    l_methods = {}

    stmt.l_methods.each do |l_method|
      function = LoxFunction.new(
        l_method,
        @@environment,
        l_method.name.lexeme == 'init'
      )
      l_methods[l_method.name.lexeme] = function
    end

    klass = LoxClass.new(stmt.name.lexeme, superclass, l_methods)

    unless superclass.nil?
      @@environment = @@environment.enclosing
    end

    @@environment.assign(stmt.name, klass)
    return
  end

  # @param [Stmt::Print] stmt
  def visit_print_stmt(stmt)
    value = evaluate(stmt.expression)
    puts stringify(value)
  end

  # @param [Stmt::Return] stmt
  def visit_return_stmt(stmt)
    value = nil
    value = evaluate(stmt.value) unless stmt.value.nil?

    raise Return, value
  end

  # @param [Stmt::Var] stmt
  def visit_var_stmt(stmt)
    value = nil
    
    unless stmt.initializer.nil?
      value = evaluate(stmt.initializer)
    end

    @@environment.define(stmt.name.lexeme, value)

    return nil
  end

  # @param [Stmt::While] stmt
  def visit_while_stmt(stmt)
    while evaluate(stmt.condition)
      execute(stmt.body)
    end
    return
  end

  private

  # @param [Expr] expr
  def evaluate(expr) = expr.accept(self)

  # @param [Stmt] stmt
  def execute(stmt)
    stmt.accept(self)
  end

  # @param [Object] object
  def stringify(object)
    return 'nil' if object.nil?

    text = object.to_s

    if object.is_a?(Float)
      text = text[0...text.length - 2] if text.end_with?('.0')

      return text
    end

    text
  end

  # @param [Token] operator
  # @param [Object] operand
  def check_number_operand(operator, operand)
    return if operand.is_a?(Float)

    raise RuntimeError.new('The operand must be a number', operator.lexeme)
  end

  # @param [Token] operator
  # @param [Object] left
  # @param [Object] right
  def check_number_operands(operator, left, right)
    return if left.is_a?(Float) && right.is_a?(Float)

    raise RuntimeError.new('Operands must be numbers', operator.lexeme)
  end
end
