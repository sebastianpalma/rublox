# frozen_string_literal: true

require_relative './errors/parser_error'
require_relative './expr/assign'
require_relative './expr/binary'
require_relative './expr/call'
require_relative './expr/get'
require_relative './expr/grouping'
require_relative './expr/literal'
require_relative './expr/set'
require_relative './expr/super'
require_relative './expr/this'
require_relative './expr/unary'
require_relative './expr/variable'
require_relative './stmt/block'
require_relative './stmt/expression'
require_relative './stmt/function'
require_relative './stmt/if'
require_relative './stmt/l_class'
require_relative './stmt/print'
require_relative './stmt/var'
require_relative './stmt/return'
require_relative './stmt/while'
require_relative './token_types'

class Parser
  def initialize(tokens)
    @current = 0
    @tokens = tokens
  end

  def parse
    statements = []
    statements.push(declaration) until at_end?
    statements
  end

  private

  def expression() = assignment

  def declaration
    return class_declaration if match(TokenTypes::CLASS)
    return function('function') if match(TokenTypes::FUN)
    return var_declaration if match(TokenTypes::VAR)

    statement
  rescue ParserError
    synchronize
    return
  end

  def class_declaration
    name = consume(TokenTypes::IDENTIFIER, 'Expect class name.')

    superclass = nil

    if match(TokenTypes::LESS)
      consume(TokenTypes::IDENTIFIER, 'Expect superclass name.')
      superclass = Expr::Variable.new(previous)
    end

    consume(TokenTypes::LEFT_BRACE, "Expect '{' before class body.")

    methods = []

    while !check(TokenTypes::RIGHT_BRACE) && !at_end?
      methods.push(function('method'))
    end

    consume(TokenTypes::RIGHT_BRACE, "Expect '}' after class body.")

    Stmt::LClass.new(name, superclass, methods)
  end

  def statement
    return for_statement if match(TokenTypes::FOR)
    return if_statement if match(TokenTypes::IF)
    return print_statement if match(TokenTypes::PRINT)
    return return_statement if match(TokenTypes::RETURN)
    return while_statement if match(TokenTypes::WHILE)
    return Stmt::Block.new(block) if match(TokenTypes::LEFT_BRACE)

    expression_statement
  end

  def for_statement
    consume(TokenTypes::LEFT_PAREN, "Expect '(' after 'for'.")

    initializer = if match(TokenTypes::SEMICOLON)
                    nil
                  elsif match(TokenTypes::VAR)
                    var_declaration
                  else
                    expression_statement
                  end

    unless check(TokenTypes::SEMICOLON)
      condition = expression
    end

    consume(TokenTypes::SEMICOLON, "Expect ';' after loop condition.")

    unless check(TokenTypes::RIGHT_PAREN)
      increment = expression
    end

    consume(TokenTypes::RIGHT_PAREN, "Expect ')' after for clauses.")

    body = statement

    unless increment.nil?
      body = Stmt::Block.new([body, Stmt::Expression.new(increment)])
    end

    condition = Expr::Literal.new(true) if condition.nil?

    body = Stmt::While.new(condition, body)

    unless initializer.nil?
      body = Stmt::Block.new([initializer, body])
    end

    body
  end

  def function(kind)
    name = consume(TokenTypes::IDENTIFIER, "Expect #{kind} name.")
    
    consume(TokenTypes::LEFT_PAREN, "Expect '(' after #{kind} name.")

    parameters = []

    if !check(TokenTypes::RIGHT_PAREN)
      begin 
        if parameters.size >= 255
          error(peek, "Can't have more than 255 parameters.")
        end

        parameters.push(
          consume(TokenTypes::IDENTIFIER, "Expect parameter name.")
        )
      end while match(TokenTypes::COMMA)
    end

    consume(TokenTypes::RIGHT_PAREN, "Expect ')' after parameters.")
    consume(TokenTypes::LEFT_BRACE, "Expect '{' before #{kind} body.")

    body = block

    Stmt::Function.new(name, parameters, body)
  end

  def if_statement
    consume(TokenTypes::LEFT_PAREN, "Expect '(' after 'if'.")
    condition = expression
    consume(TokenTypes::RIGHT_PAREN, "Expect ')' after 'if' condition.")

    then_branch = statement
    else_branch = nil
    else_branch = statement if match(TokenTypes::ELSE)

    Stmt::If.new(condition, then_branch, else_branch)
  end

  def return_statement
    keyword = previous
    value = nil
    value = expression unless check(TokenTypes::SEMICOLON)

    consume(TokenTypes::SEMICOLON, "Expect ';' after return value.")

    Stmt::Return.new(keyword, value)
  end

  def while_statement
    consume(TokenTypes::LEFT_PAREN, "Expect '(' after 'while'.")
    condition = expression 
    consume(TokenTypes::RIGHT_PAREN, "Expect ')' after condition.")

    body = statement

    Stmt::While.new(condition, body)
  end

  def print_statement
    value = expression
    consume(TokenTypes::SEMICOLON, "Expect ';' after value.")
    Stmt::Print.new(value)
  end

  def var_declaration
    name = consume(TokenTypes::IDENTIFIER, 'Expect variable name.')
    initializer = nil
    initializer = expression if match(TokenTypes::EQUAL)

    consume(TokenTypes::SEMICOLON, "Expect ';' after variable declaration.")

    Stmt::Var.new(name, initializer)
  end

  def expression_statement
    expr = expression
    consume(TokenTypes::SEMICOLON, "Expect ';' after expression.")
    Stmt::Expression.new(expr)
  end

  def block
    statements = []

    while !check(TokenTypes::RIGHT_BRACE) && !at_end?
      statements.push(declaration)
    end

    consume(TokenTypes::RIGHT_BRACE, "Expect '}' after block.")

    statements
  end

  def assignment
    expr = conditional_or

    if match(TokenTypes::EQUAL)
      equals = previous
      value = assignment

      if expr.is_a?(Expr::Variable)
        name = expr.name
        return Expr::Assign.new(name, value)
      # [TODO]: test.
      elsif expr.is_a?(Expr::Get)
        get = expr
        return Expr::Set.new(get.object, get.name, value)
      end

      error(equals, 'Invalid assignment target.')
    end

    expr
  end

  def conditional_or
    expr = conditional_and

    while match(TokenTypes::OR)
      operator = previous
      right = conditional_and
      expr = Expr::Logical.new(expr, operator, right)
    end

    expr
  end

  def conditional_and
    expr = equality

    while match(TokenTypes::AND)
      operator = previous
      right = equality
      expr = Expr::Logical.new(expr, operator, right)
    end

    expr
  end

  def equality
    expr = comparison

    while match(TokenTypes::BANG_EQUAL, TokenTypes::EQUAL_EQUAL)
      operator = previous
      right = comparison
      expr = Expr::Binary.new(expr, operator, right)
    end

    expr
  end

  def comparison
    expr = term

    while match(TokenTypes::GREATER, TokenTypes::GREATER_EQUAL, TokenTypes::LESS, TokenTypes::LESS_EQUAL)
      operator = previous
      right = term
      expr = Expr::Binary.new(expr, operator, right)
    end

    expr
  end

  def term
    expr = factor

    while match(TokenTypes::MINUS, TokenTypes::PLUS)
      operator = previous
      right = factor
      expr = Expr::Binary.new(expr, operator, right)
    end

    expr
  end

  def factor
    expr = unary

    while match(TokenTypes::SLASH, TokenTypes::STAR)
      operator = previous
      right = unary
      expr = Expr::Binary.new(expr, operator, right)
    end

    expr
  end

  def unary
    if match(TokenTypes::BANG, TokenTypes::MINUS)
      operator = previous
      right = unary
      return Expr::Unary.new(operator, right)
    end

    call
  end

  def call
    expr = primary

    while true
      if match(TokenTypes::LEFT_PAREN)
        expr = finish_call(expr)
      # [TODO]: test.
      elsif match(TokenTypes::DOT)
        name = consume(TokenTypes::IDENTIFIER, "Expect property name after '.'.")
        expr = Expr::Get.new(expr, name)
      else
        break
      end
    end

    expr
  end

  def finish_call(callee)
    arguments = []

    if !check(TokenTypes::RIGHT_PAREN)
      begin
        if arguments.size >= 255
          error(peek(), "Can't have more than 255 arguments.")
        end

        arguments.push(expression)
      end while match(TokenTypes::COMMA)
    end

    paren = consume(TokenTypes::RIGHT_PAREN, "Expect ')' after arguments.")

    Expr::Call.new(callee, paren, arguments)
  end

  def match(*types)
    types.each do |type|
      if check(type)
        advance
        return true
      end
    end

    false
  end

  def primary
    return Expr::Literal.new(false) if match(TokenTypes::FALSE)
    return Expr::Literal.new(true) if match(TokenTypes::TRUE)
    return Expr::Literal.new(nil) if match(TokenTypes::NIL)

    if match(TokenTypes::NUMBER, TokenTypes::STRING)
      return Expr::Literal.new(previous.literal)
    end

    if match(TokenTypes::SUPER)
      keyword = previous
      consume(TokenTypes::DOT, "Expect '.' after 'super'.")
      l_method = consume(TokenTypes::IDENTIFIER, "Expect superclass method name.")

      return Expr::Super.new(keyword, l_method)
    end

    if match(TokenTypes::THIS)
      return Expr::This.new(previous)
    end

    if match(TokenTypes::IDENTIFIER)
      return Expr::Variable.new(previous)
    end

    if match(TokenTypes::LEFT_PAREN)
      expr = expression
      consume(TokenTypes::RIGHT_PAREN, "Expect ')' after expression.")
      return Expr::Grouping.new(expr)
    end

    raise error(peek, 'Expect expression.')
  end

  def check(type)
    return false if at_end?

    peek.type == type
  end

  def peek() = @tokens[@current]

  def advance
    @current += 1 unless at_end?
    previous
  end

  def previous() = @tokens[@current - 1]

  def error(token, message)
    Lox.error(token, message)
    ParserError.new
  end

  def consume(type, message)
    return advance if check(type)

    raise error(peek, message)
  end

  def at_end?() = peek.type == TokenTypes::EOF

  def synchronize
    advance

    until at_end?
      return if previous.type == TokenTypes::SEMICOLON

      case peek.type
      when TokenTypes::CLASS
      when TokenTypes::FUN
      when TokenTypes::VAR
      when TokenTypes::FOR
      when TokenTypes::IF
      when TokenTypes::WHILE
      when TokenTypes::PRINT
      when TokenTypes::RETURN
        return
      end

      advance
    end
  end
end
