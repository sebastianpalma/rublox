# frozen_string_literal: true

require 'pry'

require_relative './interpreter'
require_relative './parser'
require_relative './resolver'
require_relative './scanner'

class Lox
  # @attr_reader [Array<String>] args
  # @attr_reader [Interpreter] interpreter
  def initialize(args)
    @file = args[0]
    @interpreter = Interpreter.new
    @@error = false
  end

  class << self
    def error(token, message)
      if token.type == TokenTypes::EOF
        report(token.line, ' at end', message)
      else
        report(token.line, " at '#{token.lexeme}'", message)
      end
    end

    def report(line, where, message)
      puts "[line #{line}] Error#{where}: #{message}"
      @@error = true
    end

    def runtime_error(error)
      puts "#{error}\n[line #{error.token.line}]"
    end
  end

  def main() = run_file

  private

  attr_reader :file, :interpreter

  def run_file
    had_error = run(IO.read(file))

    exit if had_error == true
  end

  def run(source)
    tokens, errored, _error = Scanner.new(source).scan_tokens
    parser = Parser.new(tokens)
    statements = parser.parse

    return true if errored

    resolver = Resolver.new(interpreter)
    resolver.resolve(statements)

    return if @@error == true

    interpreter.interpret(statements)
  end
end

Lox.new(ARGV).main unless ENV['ENV'] == 'test'
