# frozen_string_literal: true

require_relative './lox_instance'

class LoxClass
  # @attr_reader [String] name
  # @attr_reader [LoxClass] superclass
  # @attr_reader [Hash<String, LoxFunction>] l_methods
  attr_reader :name, :superclass, :l_methods

  def initialize(name, superclass, l_methods)
    @name = name
    @superclass = superclass
    @l_methods = l_methods
  end

  def to_s
    name
  end

  # @param [String] name
  #
  # @return [LoxFunction]
  def find_method(name)
    return l_methods[name] if l_methods.key?(name)
    return superclass.find_method(name) unless superclass.nil?
  end

  # @param [Interpreter] interpreter
  # @param [Array<Object>] arguments
  #
  # @return [LoxInstance]
  def call(interpreter, arguments)
    instance = LoxInstance.new(self)
    initializer = find_method('init')

    unless initializer.nil?
      initializer.bind(instance).call(interpreter, arguments)
    end

    instance
  end

  # @return [Integer]
  def arity
    initializer = find_method('init')
    return 0 if initializer.nil?
    initializer.arity
  end

  def ==(other)
    return false unless other.is_a?(LoxClass)

    name == other.name &&
      superclass == other.superclass &&
      l_methods == other.l_methods
  end
end
