# frozen_string_literal: true

module Stmt
  class Var
    # @attr_reader [Token] name
    # @attr_reader [Expr] initializer
    attr_reader :name, :initializer

    def initialize(name, initializer)
      @name = name
      @initializer = initializer
    end

    # @param [Interpreter] visitor
    def accept(visitor)
      visitor.visit_var_stmt(self)
    end

    def to_s
      "var #{name} = #{initializer};"
    end

    # @param [Stmt::Var] other
    #
    # @return [Boolean]
    def ==(other)
      return false unless other.is_a?(Stmt::Var)

      name == other.name && initializer == other.initializer
    end
  end
end
