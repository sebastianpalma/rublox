# frozen_string_literal: true

module Stmt
  class Expression
    # @attr_reader [Expr] expression
    attr_reader :expression

    def initialize(expression)
      @expression = expression
    end

    # @param [Interpreter] visitor
    def accept(visitor)
      visitor.visit_expression_stmt(self)
    end

    # @param [Stmt::Expression] other
    #
    # @return [Boolean]
    def ==(other)
      return false unless other.is_a?(Stmt::Expression)

      expression == other.expression
    end
  end
end
