# frozen_string_literal: true

module Stmt
  class Return
    # @attr_reader [Token] keyword
    # @attr_reader [Expr] value
    attr_reader :keyword, :value

    def initialize(keyword, value)
      @keyword = keyword
      @value = value
    end

    # @param [Interpreter] visitor
    def accept(visitor)
      visitor.visit_return_stmt(self)
    end

    def to_s
      "#{keyword} #{value};"
    end

    # @param [Stmt::Var] other
    #
    # @return [Boolean]
    def ==(other)
      return false unless other.is_a?(Stmt::Return)

      keyword == other.keyword && value == other.value
    end
  end
end
