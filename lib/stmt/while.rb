# frozen_string_literal: true

module Stmt
  class While
    # @attr_reader [Expr] condition
    # @attr_reader [Stmt] body
    attr_reader :condition, :body

    def initialize(condition, body)
      @condition = condition
      @body = body
    end

    # @param [Interpreter] visitor
    def accept(visitor)
      visitor.visit_while_stmt(self)
    end

    def to_s
      "while (#{condition}) {\n  #{body}\n}"
    end

    # @param [Stmt::While] other
    #
    # @return [Boolean]
    def ==(other)
      return false unless other.is_a?(Stmt::While)

      condition == other.condition && body == other.body
    end
  end
end
