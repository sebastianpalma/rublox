# frozen_string_literal: true

module Stmt
  class Function
    # @attr_reader [Array<Stmt>] body
    # @attr_reader [Token] name
    # @attr_reader [Array<Token>] params
    attr_reader :body, :name, :params

    def initialize(name, params, body)
      @body = body
      @name = name
      @params = params
    end

    # @param [Interpreter] visitor
    def accept(visitor)
      visitor.visit_function_stmt(self)
    end

    def to_s
      stringified_params = params.map(&:to_s).join(", ")
      stringified_body = body.map { |line| "  #{line}" }.join("\n")

      <<~STR
        fun #{name}(#{stringified_params}) {
          #{stringified_body}
        }
      STR
    end

    # @param [Stmt::Function] other
    #
    # @return [Boolean]
    def ==(other)
      return false unless other.is_a?(Stmt::Function)

      name == other.name &&
        params == other.params &&
        body == other.body
    end
  end
end

