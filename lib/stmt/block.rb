# frozen_string_literal: true

module Stmt
  class Block
    # @attr_reader [Array<Token>] statements
    attr_reader :statements

    def initialize(statements)
      @statements = statements
    end

    # @param [Interpreter] visitor
    def accept(visitor)
      visitor.visit_block_stmt(self)
    end

    def to_s
      statements.map { |line| "  #{line}" }.join("\n")
    end

    # @param [Stmt::Block] other
    #
    # @return [Boolean]
    def ==(other)
      return false unless other.is_a?(Stmt::Block)

      statements == other.statements
    end
  end
end
