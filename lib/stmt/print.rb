# frozen_string_literal: true

module Stmt
  class Print
    # @attr_reader [Expr] expression
    attr_reader :expression

    def initialize(expression)
      @expression = expression
    end

    # @param [Interpreter] visitor
    def accept(visitor)
      visitor.visit_print_stmt(self)
    end

    # @param [Stmt::Print] other
    #
    # @return [Boolean]
    def ==(other)
      return false unless other.is_a?(Stmt::Print)

      expression == other.expression
    end
  end
end
