# frozen_string_literal: true

module Stmt
  class If
    # @attr_reader [Expr] condition
    # @attr_reader [Stmt] then_branch
    # @attr_reader [Stmt] else_branch
    attr_reader :condition, :then_branch, :else_branch

    def initialize(condition, then_branch, else_branch)
      @condition = condition
      @then_branch = then_branch
      @else_branch = else_branch
    end

    # @param [Interpreter] visitor
    def accept(visitor)
      visitor.visit_if_stmt(self)
    end

    def to_s
      if else_branch.nil?
        "if (#{condition}) {\n  #{then_branch}\n}"
      else
        "if (#{condition}) {\n  #{then_branch}\n} else {\n  #{else_branch}\n}"
      end
    end

    # @param [Stmt::If] other
    #
    # @return [Boolean]
    def ==(other)
      return false unless other.is_a?(Stmt::If)

      condition == other.condition &&
        then_branch == other.then_branch &&
        else_branch == other.else_branch
    end
  end
end
