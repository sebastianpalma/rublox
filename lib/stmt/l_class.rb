# frozen_string_literal: true

module Stmt
  class LClass
    # @attr_reader [Token] name
    # @attr_reader [Expr::Variable] superclass
    # @attr_reader [Array<Stmt::Function>] l_methods
    attr_reader :name, :superclass, :l_methods

    def initialize(name, superclass, l_methods)
      @name = name
      @superclass = superclass
      @l_methods = l_methods
    end

    # @param [Interpreter] visitor
    def accept(visitor)
      visitor.visit_l_class_stmt(self)
    end

    # @param [Stmt::Block] other
    #
    # @return [Boolean]
    def ==(other)
      return false unless other.is_a?(Stmt::LClass)

      name == other.name &&
        superclass == other.superclass &&
        l_methods == other.l_methods
    end
  end
end
