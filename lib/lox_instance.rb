# frozen_string_literal: true

class LoxInstance
  # @attr_reader [LoxClass] klass
  attr_reader :klass

  # attr_reader [Hash<String, Object>] fields
  @@fields = {}

  class << self
    def fields
      @@fields
    end

    def fields=(other)
      @@fields = other
    end
  end

  def fields
    self.class.fields
  end

  def fields=(other)
    self.class.fields = other
  end

  def initialize(klass)
    @klass = klass
  end

  # @param [Token] name
  #
  # @return Object
  def get(name)
    if fields.key?(name.lexeme)
      return fields[name.lexeme]
    end

    # [TODO]: test.
    l_method = klass.find_method(name.lexeme)
    return l_method.bind(self) unless l_method.nil?

    raise StandardError, "Undefined property #{name.lexeme}."
  end

  # @param [Token] name
  # @param [Object] value
  #
  # @return [Object]
  def set(name, value)
    fields[name.lexeme] = value
  end

  def to_s
    "#{klass.name} instance"
  end

  def ==(other)
    return false unless other.is_a?(LoxInstance)

    klass == other.klass
  end
end
