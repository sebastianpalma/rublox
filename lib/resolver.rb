# frozen_string_literal: true

class Resolver
  FUNCTION_TYPES = [:NONE, :FUNCTION, :METHOD, :INITIALIZER].freeze
  CLASS_TYPES = [:NONE, :CLASS, :SUBCLASS].freeze

  attr_reader :current_function, :interpreter

  @@scopes = []
  @@current_function = FUNCTION_TYPES[0]
  @@current_class = CLASS_TYPES[0]

  class << self
    def scopes
      @@scopes
    end

    def scopes=(other)
      @@scopes = other
    end
  end

  def initialize(interpreter)
    @interpreter = interpreter
  end

  # @param [Array<Stmt>] args
  # @param [Stmt] args
  def resolve(args)
    if args.is_a?(Array)
      statements = args
      statements.each { |stmt| resolve(stmt) }
    elsif args.class.name[/(?<=^)(.*)(?=::)/] == 'Stmt'
      stmt = args
      stmt.accept(self)
    elsif args.class.name[/(?<=^)(.*)(?=::)/] == 'Expr'
      expr = args
      expr.accept(self)
    end
  end

  # @return [Nil]
  def begin_scope
    @@scopes.push({})
  end

  # @return [Nil]
  def end_scope
    @@scopes.pop
  end

  # @param [Token] name
  #
  # @return [Nil]
  def declare(name)
    return if @@scopes.empty?

    scope = @@scopes.pop

    if scope.key?(name.lexeme)
      Lox.error(name, 'Already a variable with this name in this scope.')
    end

    scope[name.lexeme] = false
    @@scopes.push(scope)

    return
  end

  # @param [Token] name
  def define(name)
    return if @@scopes.empty?

    scope = @@scopes.pop
    scope[name.lexeme] = true
    @@scopes.push(scope)
  end

  # @param [Expr] expr
  # @param [Token] name
  #
  # @return [Nil]
  def resolve_local(expr, name)
    lexeme = name.lexeme

    @@scopes
      .reverse
      .select { |scope| scope.key?(lexeme) }
      .each.with_index { |_, index| @interpreter.resolve(expr, index) }

    # (@@scopes.size - 1).upto(1).each do |i|
    #   if (@@scopes[i] || {}).key?(lexeme)
    #     @interpreter.resolve(expr, @@scopes.size - 1 - i)
    #   end
    # end

    return
  end

  # @param [Stmt::Function] function
  # @param [Resolver::FUNCTION_TYPES] type
  def resolve_function(function, type)
    enclosing_function = @@current_function
    @@current_function = type
    begin_scope

    function.params.each do |param|
      declare(param)
      define(param)
    end

    resolve(function.body)
    end_scope
    @@current_function = enclosing_function
  end

  # @param [Stmt::Block] stmt
  def visit_block_stmt(stmt)
    begin_scope
    resolve(stmt.statements)
    end_scope
    return
  end

  # @param [Stmt::LClass] stmt
  def visit_l_class_stmt(stmt)
    enclosing_class = @@current_class
    @@current_class = CLASS_TYPES[1]

    declare(stmt.name)
    define(stmt.name)

    unless stmt.superclass.nil?
      if stmt.name.lexeme == stmt.superclass.name.lexeme
        Lox.error(stmt.superclass.name, "A class can't inherit from itself.")
      end
    end

    unless stmt.superclass.nil?
      @@current_class = CLASS_TYPES[2]
      resolve(stmt.superclass)
    end

    unless stmt.superclass.nil?
      begin_scope

      scope = @@scopes.pop
      scope['super'] = true
      @@scopes.push(scope)
    end

    begin_scope
    
    scope = @@scopes.pop
    scope['this'] = true
    @@scopes.push(scope)

    # [TODO]: test.
    stmt.l_methods.each do |l_method|
      declaration = FUNCTION_TYPES[2]
      declaration = FUNCTION_TYPES[3] if l_method.name.lexeme == 'init'

      resolve_function(l_method, declaration)
    end

    end_scope
    end_scope unless stmt.superclass.nil?

    @@current_class = enclosing_class

    return
  end

  # @param [Stmt::Expression] stmt
  def visit_expression_stmt(stmt)
    resolve(stmt.expression)
    return
  end

  # @param [Stmt::Function] stmt
  def visit_function_stmt(stmt)
    declare(stmt.name)
    define(stmt.name)
    resolve_function(stmt, FUNCTION_TYPES[1])
    return
  end

  # @param [Stmt::If] stmt
  def visit_if_stmt(stmt)
    resolve(stmt.condition)
    resolve(stmt.then_branch)

    resolve(stmt.else_branch) unless stmt.else_branch.nil?

    return
  end

  # @param [Stmt::Print] stmt
  def visit_print_stmt(stmt)
    resolve(stmt.expression)
    return
  end

  # @param [Stmt::Return] stmt
  def visit_return_stmt(stmt)
    if @@current_function == FUNCTION_TYPES[0]
      Lox.error(stmt.keyword, "Can't return from top-level code.")
    end

    unless stmt.value.nil?
      if @@current_function == FUNCTION_TYPES[3]
        Lox.error(stmt.keyword, "Can't return a value from an initializer.")
      end

      resolve(stmt.value)
    end

    return
  end

  # @param [Stmt::Var] stmt
  def visit_var_stmt(stmt)
    declare(stmt.name)

    unless stmt.initializer.nil?
      resolve(stmt.initializer)
    end

    define(stmt.name)

    return
  end

  # @param [Stmt::While] stmt
  def visit_while_stmt(stmt)
    resolve(stmt.condition)
    resolve(stmt.body)
    return
  end

  # @param [Expr::Assign] expr
  def visit_assign_expr(expr)
    resolve(expr.value)
    resolve_local(expr, expr.name)
    return
  end

  # @param [Expr::Binary] expr
  def visit_binary_expr(expr)
    resolve(expr.left)
    resolve(expr.right)
    return
  end

  # @param [Expr::Call] expr
  def visit_call_expr(expr)
    resolve(expr.callee)

    expr.arguments.each do |argument|
      resolve(argument)
    end

    return
  end

  # [TODO]: test.
  # @param [Expr::Get] expr
  def visit_get_expr(expr)
    resolve(expr.object)
    return
  end

  # @param [Expr::Grouping] expr
  def visit_grouping_expr(expr)
    resolve(expr.expression)
    return
  end

  # @param [Expr::Literal] expr
  def visit_literal_expr(expr)
    return
  end

  # @param [Expr::Logical] expr
  def visit_logical_expr(expr)
    resolve(expr.left)
    resolve(expr.right)
    return
  end

  # [TODO]: test.
  # @param [Expr::Set] expr
  def visit_set_expr(expr)
    resolve(expr.value)
    resolve(expr.object)
    return
  end

  # [TODO]: test.
  # @param [Expr::Super] expr
  def visit_super_expr(expr)
    if @@current_class == CLASS_TYPES[0]
      Lox.error(expr.keyword, "Can't use 'super' outside of a class.")
    end

    if @@current_class != CLASS_TYPES[2]
      Lox.error(expr.keyword, "Can't use 'super' in a class with no superclass.")
    end

    resolve_local(expr, expr.keyword)

    return
  end

  # [TODO]: test.
  # @param [Expr::This] expr
  def visit_this_expr(expr)
    if @@current_class == CLASS_TYPES[0]
      Lox.error(expr.keyword, "Can't use 'this' outside of a class.")
    end

    resolve_local(expr, expr.keyword)
    return
  end

  # @param [Expr::Unary] expr
  def visit_unary_expr(expr)
    resolve(expr.right)
    return
  end

  # @param [Expr::Variable] expr
  def visit_variable_expr(expr)
    if !@@scopes.empty? && @@scopes.pop[expr.name.lexeme] == false
      Lox.error(expr.name, "Can't read local variable in its own initializer.")
    end

    resolve_local(expr, expr.name)
    return
  end
end
