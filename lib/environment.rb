# frozen_string_literal: true

require_relative './errors/runtime_error'

class Environment
  # @attr_reader [Environment] enclosing
  attr_reader :enclosing

  # @attr_reader [Hash<String, <Boolean, Float, String>>] values
  @@values = {}

  class << self
    def values
      @@values
    end

    def values=(other)
      @@values = other
    end
  end

  def values
    self.class.values
  end

  def values=(other)
    self.class.values = other
  end

  def initialize(enclosing = nil)
    @enclosing = enclosing
  end

  # @param [Token] name
  def get(name)
    if @@values.key?(name.lexeme)
      return @@values[name.lexeme]
    end

    return @enclosing.get(name) unless @enclosing.nil?

    raise RuntimeError.new("Undefined variable '#{name.lexeme}'.", name)
  end

  # @param [Token] name
  # @param [Object] value
  def assign(name, value)
    if @@values.key?(name.lexeme)
      @@values[name.lexeme] = value
      return
    end

    if enclosing.is_a?(Environment)
      enclosing.assign(name, value)
      return
    end

    raise RuntimeError.new("Undefined variable '#{name.lexeme}'.", name)
  end

  # @param [String] name
  # @param [Object] value
  def define(name, value)
    @@values[name] = value
  end

  # @param [Integer] distance
  #
  # @return [Environment]
  def ancestor(distance)
    environment = self

    distance.times do
      environment = environment.enclosing
    end

    environment
  end

  # @param [Integer] distance
  # @param [String] name
  def get_at(distance, name)
    ancestor(distance).class.values[name]
  end

  # @param [Integer] distance
  # @param [Token] name
  # @param [Object] value
  def assign_at(distance, name, value)
    ancestor(distance).class.values[name.lexeme] = value
  end

  # @param [Environment] other
  #
  # @return [Boolean]
  def ==(other)
    enclosing == other.enclosing &&
      self.class.values == other.class.values
  end
end
