class LoxFunction
  attr_reader :declaration, :closure, :is_initializer

  # @attr_reader [Environment] closure
  # @attr_reader [Stmt::Function] declaration
  # @attr_reader [Bool] is_initializer
  def initialize(declaration, closure, is_initializer)
    @declaration = declaration
    @closure = closure
    @is_initializer = is_initializer
  end

  # @param [Interpreter] interpreter
  # @param [Array<Object>] arguments
  def call(interpreter, arguments)
    environment = Environment.new(closure)

    declaration.params.each_with_index do |param, index|
      environment.define(param.lexeme, arguments[index])
    end

    begin
      interpreter.execute_block(declaration.body, environment);
    rescue Return => return_value
      return closure.get_at(0, 'this') if is_initializer == true

      return return_value.value
    end

    return closure.get_at(0, 'this') if is_initializer == true

    nil
  end

  # @return [Integer]
  def arity
    declaration.params.size
  end

  # [TODO]: test.
  # @param [LoxInstance] instance
  def bind(instance)
    environment = Environment.new(closure)
    environment.define('this', instance)
    LoxFunction.new(declaration, environment, is_initializer)
  end

  # @return [String]
  def to_s
    "<fn #{ declaration.name.lexeme }>"
  end

  def ==(other)
    return false unless other.is_a?(LoxFunction)

    closure == other.closure &&
      declaration == other.declaration
  end
end
