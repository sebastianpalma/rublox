# frozen_string_literal: true

require_relative './keywords'
require_relative './token'
require_relative './token_types'

class Scanner
  def initialize(source)
    @current = 0
    @line = 1
    @source = source
    @start = 0
    @tokens = []
  end

  # return [Array<Token>, Boolean, String]
  def scan_tokens
    had_error = false
    error = ''

    until at_end?
      @start = @current

      errored, message = scan_token

      if errored == true
        had_error = true
        error = message
      end
    end

    @tokens.push(Token.new('', @line, nil, TokenTypes::EOF))
    [@tokens, had_error, error]
  end

  attr_reader :source

  private

  def at_end?
    @current >= source.length
  end

  # return [Boolean] whether there was an unexpected token or not.
  def scan_token
    case c = advance
    when '(' then add_token(TokenTypes::LEFT_PAREN)
    when ')' then add_token(TokenTypes::RIGHT_PAREN)
    when '{' then add_token(TokenTypes::LEFT_BRACE)
    when '}' then add_token(TokenTypes::RIGHT_BRACE)
    when ',' then add_token(TokenTypes::COMMA)
    when '.' then add_token(TokenTypes::DOT)
    when '-' then add_token(TokenTypes::MINUS)
    when '+' then add_token(TokenTypes::PLUS)
    when ';' then add_token(TokenTypes::SEMICOLON)
    when '*' then add_token(TokenTypes::STAR)
    when '!'
      return add_token(TokenTypes::BANG_EQUAL) if match?('=')
      add_token(TokenTypes::BANG)
    when '='
      return add_token(TokenTypes::EQUAL_EQUAL) if match?('=')
      add_token(TokenTypes::EQUAL)
    when '<'
      return add_token(TokenTypes::LESS_EQUAL) if match?('=')
      add_token(TokenTypes::LESS)
    when '>'
      return add_token(TokenTypes::GREATER_EQUAL) if match?('=')
      add_token(TokenTypes::GREATER)
    when '/'
      if match?('/')
        advance while peek != '\n' && !at_end?
      else
        add_token(TokenTypes::SLASH)
      end
    when ' '  then [false, '']
    when "\r" then [false, '']
    when "\t" then [false, '']
    when "\n"
      @line += 1
      [false, '']
    when '"' then string
    else
      return number if digit?(c)
      return identifier if alpha?(c)

      [true, "[line #{@line}] Error : Unexpected character."]
    end
  end

  def string
    while peek != '"' && !at_end?
      @line += 1 if peek == "\n"
      advance
    end

    return [true, 'Unterminated string.'] if at_end?

    # The closing ".
    advance

    # Trim the surrounding quotes.
    add_token(TokenTypes::STRING, source[@start.next...@current.pred])
  end

  def number
    advance while digit?(peek)

    if peek == '.' && digit?(peek_next)
      advance
      advance while digit?(peek)
    end

    add_token(TokenTypes::NUMBER, source[@start...@current].to_f)
  end

  def advance
    source[(@current += 1).pred]
  end

  def add_token(token_type, literal = nil)
    @tokens.push(
      Token.new(
        source[@start...@current],
        @line,
        literal,
        token_type
      )
    )
    [false, ''] # no error was found
  end

  def digit?(char)
    char >= '0' && char <= '9'
  end

  def alpha?(char)
    (char >= 'a' && char <= 'z') || (char >= 'A' && char <= 'Z') || char == '_'
  end

  def alphanumeric?(char)
    alpha?(char) || digit?(char)
  end

  def identifier
    advance while alphanumeric?(peek)

    text = source[@start...@current]
    token_type = begin
                   Keywords.const_get(text.upcase)
                 rescue NameError
                   TokenTypes::IDENTIFIER
                 end

    add_token(token_type)
  end

  def peek
    return '\0' if at_end?

    source[@current]
  end

  def peek_next
    return '\0' if @current.next >= source.length

    source[@current.next]
  end

  def match?(expected)
    return false if at_end?
    return false unless source[@current] == expected

    @current += 1

    true
  end
end
