# frozen_string_literal: true

module Expr
  class Set
    # @attr_reader [Expr] object
    # @attr_reader [Token] name
    # @attr_reader [Expr] name
    attr_reader :object, :name, :value

    def initialize(object, name, value)
      @name = name
      @object = object
      @value = value
    end

    # @param [Interpreter] visitor
    def accept(visitor)
      visitor.visit_set_expr(self)
    end

    # @param [Expr::Set] other
    #
    # @return [Boolean]
    def ==(other)
      return false unless other.is_a?(Expr::Set)

      object == other.object &&
        name == other.name &&
        value == other.value
    end
  end
end
