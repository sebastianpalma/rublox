# frozen_string_literal: true

module Expr
  class Get
    # @attr_reader [Expr] object
    # @attr_reader [Token] name
    attr_reader :object, :name

    def initialize(object, name)
      @object = object
      @name = name
    end

    # @param [Interpreter] visitor
    def accept(visitor)
      visitor.visit_get_expr(self)
    end

    # @param [Expr::Get] other
    #
    # @return [Boolean]
    def ==(other)
      return false unless other.is_a?(Expr::Get)

      object == other.object && name == other.name
    end
  end
end
