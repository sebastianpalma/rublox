# frozen_string_literal: true

module Expr
  class Logical
    # @attr_reader [Expr] left
    # @attr_reader [Token] operator
    # @attr_reader [Expr] right
    attr_reader :left, :operator, :right

    def initialize(left, operator, right)
      @left = left
      @operator = operator
      @right = right
    end

    # @param [Interpreter] visitor
    def accept(visitor)
      visitor.visit_logical_expr(self)
    end

    # @param [Expr::Logical] other
    #
    # @return [Boolean]
    def ==(other)
      return false unless other.is_a?(Expr::Logical)

      left == other.left &&
        operator == other.operator &&
        right == other.right
    end
  end
end
