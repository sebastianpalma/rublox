# frozen_string_literal: true

module Expr
  class Unary
    # @attr_reader [Token] operator
    # @attr_reader [Expr] right
    attr_reader :operator, :right

    def initialize(operator, right)
      @operator = operator
      @right = right
    end

    # @param [Interpreter] visitor
    def accept(visitor)
      visitor.visit_unary_expr(self)
    end

    # @param [Expr::Unary] other
    #
    # @return [Boolean]
    def ==(other)
      return false unless other.is_a?(Expr::Unary)

      operator == other.operator &&
        right == other.right
    end
  end
end
