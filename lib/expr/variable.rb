# frozen_string_literal: true

module Expr
  class Variable
    # attr_reader [Token] name
    attr_reader :name

    def initialize(name)
      @name = name
    end

    # @param [Interpreter] visitor
    def accept(visitor)
      visitor.visit_variable_expr(self)
    end

    def to_s
      name.to_s
    end

    # @param [Expr::Variable] other
    #
    # @return [Boolean]
    def ==(other)
      return false unless other.is_a?(Expr::Variable)

      name == other.name
    end
  end
end
