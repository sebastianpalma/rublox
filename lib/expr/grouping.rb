# frozen_string_literal: true

module Expr
  class Grouping
    # @attr_reader [Expr] expression
    attr_reader :expression

    def initialize(expression)
      @expression = expression
    end

    # @param [Interpreter] visitor
    def accept(visitor)
      visitor.visit_grouping_expr(self)
    end

    # @param [Expr::Grouping] other
    #
    # @return [Boolean]
    def ==(other)
      return false unless other.is_a?(Expr::Grouping)

      expression == other.expression
    end
  end
end
