# frozen_string_literal: true

module Expr
  class Literal
    # @attr_reader [Object] value
    attr_reader :value

    def initialize(value)
      @value = value
    end

    # @param [Interpreter] visitor
    def accept(visitor)
      visitor.visit_literal_expr(self)
    end

    def to_s
      value.to_s
    end

    # @param [Expr::Literal] other
    #
    # @return [Boolean]
    def ==(other)
      return false unless other.is_a?(Expr::Literal)

      value == other.value
    end
  end
end
