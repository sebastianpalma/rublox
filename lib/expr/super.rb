# frozen_string_literal: true

# [TODO]: test.
module Expr
  class Super
    # @attr_reader [Token] keyword
    # @attr_reader [Token] l_method
    attr_reader :keyword, :l_method

    def initialize(keyword, l_method)
      @keyword = keyword
      @l_method = l_method
    end

    # @param [Interpreter] visitor
    def accept(visitor)
      visitor.visit_super_expr(self)
    end

    # @param [Expr::This] other
    #
    # @return [Boolean]
    def ==(other)
      return false unless other.is_a?(Expr::Super)

      keyword == other.keyword && l_method == other.l_method
    end
  end
end
