# frozen_string_literal: true

# [TODO]: test.
module Expr
  class This
    # @attr_reader [Token] keyword
    attr_reader :keyword

    def initialize(keyword)
      @keyword = keyword
    end

    # @param [Interpreter] visitor
    def accept(visitor)
      visitor.visit_this_expr(self)
    end

    # @param [Expr::This] other
    #
    # @return [Boolean]
    def ==(other)
      return false unless other.is_a?(Expr::This)

      keyword == other.keyword
    end
  end
end
