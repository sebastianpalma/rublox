# frozen_string_literal: true

module Expr
  class Assign
    # @attr_reader [Token] name
    # @attr_reader [Expr::Literal] value
    attr_reader :name, :value

    def initialize(name, value)
      @name = name
      @value = value
    end

    # @param [Interpreter] visitor
    def accept(visitor)
      visitor.visit_assign_expr(self)
    end

    # @param [Expr::Assign] other
    #
    # @return [Boolean]
    def ==(other)
      return false unless other.is_a?(Expr::Assign)

      name == other.name && value == other.value
    end
  end
end
