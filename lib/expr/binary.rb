# frozen_string_literal: true

module Expr
  class Binary
    # @attr_reader [Expr] left
    # @attr_reader [Token] operator
    # @attr_reader [Expr] right
    attr_reader :left, :operator, :right

    def initialize(left, operator, right)
      @left = left
      @operator = operator
      @right = right
    end

    # @param [Interpreter] visitor
    def accept(visitor)
      visitor.visit_binary_expr(self)
    end

    def to_s
      "#{left} #{operator} #{right}"
    end

    # @param [Expr::Binary] other
    #
    # @return [Boolean]
    def ==(other)
      return false unless other.is_a?(Expr::Binary)

      left == other.left &&
        operator == other.operator &&
        right == other.right
    end
  end
end
