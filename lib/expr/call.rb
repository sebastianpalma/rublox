# frozen_string_literal: true

module Expr
  class Call
    # @attr_reader [Token] name
    # @attr_reader [Expr::Literal] value
    attr_reader :callee, :paren, :arguments

    def initialize(callee, paren, arguments)
      @callee = callee
      @paren = paren
      @arguments = arguments
    end

    # @param [Interpreter] visitor
    def accept(visitor)
      visitor.visit_call_expr(self)
    end

    # @param [Expr::Call] other
    #
    # @return [Boolean]
    def ==(other)
      return false unless other.is_a?(Expr::Call)

      callee == other.callee &&
        paren == other.paren &&
        arguments == other.arguments
    end
  end
end
